var connection = require('./connection_bdd');
var hashage = require('../hashage');

var ActionBdd = function(){

    this.listSpecialite = function(callback){
      connection.query('SELECT * from Specialite','none', callback);
    };

    this.listTypeStructure = function(callback){
      connection.query('SELECT * from TypeStructure','none', callback);
    };

    this.getListRempla = function(callback){
      connection.query('SELECT * from Compte C, Remplacant R WHERE C.idCompte = R.idCompte','none', function(er,row){
        if (er == null) {
            for(var i =0; i < row.length;i++){
                row[i].cleModificationMdp = 'private';
                row[i].mdp = 'private';
            }
            callback(er,row);
        } else {
            callback(er,row);
        }
      });
    };

    this.getAnnonces = function(callback){
        connection.query('SELECT * FROM Compte, LieuDeTravail WHERE idCompte = CompteLier', null, function(er,row){
            if(er == null){
                connection.query('SELECT * FROM Annonce',null, function(erAnnonce,rowAnnonce){
                    if(erAnnonce == null){
                        for(var i =0; i < row.length;i++){
                            row[i].mdp = 'private';
                            row[i].cleModificationMdp = 'private';
                            row[i].annonces = [];
                        }

                        if (typeof rowAnnonce != "undefined") {
                            for(var i =0; i < rowAnnonce.length;i++){
                                var u = 0;
                                while(typeof row[u] != "undefined" && row[u].idCompte != rowAnnonce[i].idCompteRemplace){
                                    u++;
                                }

                                if (typeof row[u] != "undefined") {row[u].annonces.push(rowAnnonce[i]);};
                            }
                        };

                        connection.query('SELECT * FROM Avis',null, function(erAvis,rowAvis){
                            if(erAvis == null){

                                if (typeof rowAvis != "undefined") {
                                    for(var i =0; i < rowAvis.length;i++){
                                        var u = 0;
                                        while(typeof row[u] != "undefined" && row[u].idCompte != rowAvis[i].idCompte2){
                                            u++;
                                        }

                                        if (typeof row[u] != "undefined") {
                                            if (typeof row[u].somme != "undefined") {
                                                row[u].somme += rowAvis[i].note;
                                                row[u].nombre++;
                                            } else {
                                                row[u].somme = rowAvis[i].note;
                                                row[u].nombre = 1;
                                            }
                                        };
                                    }
                                };

                                for(var i =0; i < row.length;i++){
                                    if(typeof row[i].nombre != "undefined") row[i].moyenne = ((row[i].somme / row[i].nombre)/5)*69;
                                    else row[i].moyenne = 0.5*69;
                                }

                                callback(null, row);
                            } else {
                                callback(erAvis);
                            }
                        });
                    } else {
                        callback(erAnnonce);
                    }
                });

            } else {
                callback(er);
            }
        });
    };

    this.getInformationLieu = function(values, callback){
        connection.query('SELECT * FROM Compte, LieuDeTravail WHERE idCompte = CompteLier AND idLieu = ?', [values.idLieu], function(er,row){
            if(er == null && typeof row[0] != "undefined"){
                row[0].mdp = 'private';
                row[0].cleModificationMdp = 'private';
                callback(null, row[0]);
            } else {
                callback(er+" or undefined");
            }
        });
    };

    this.getIdCompte = function(values, callback){
        var param = [values.email];

        connection.query('SELECT idCompte FROM Compte WHERE email = ?', param, function(er,row){
            if(er == null){
                callback(null, row[0]);
            } else {
                callback(er);
            }
        });
    };

    this.getEmail = function(values, callback){
        var param = [values.idCompte];

        connection.query('SELECT email FROM Compte WHERE idCompte = ?', param, function(er,row){
            if(er == null){
                callback(null, row[0]);
            } else {
                callback(er);
            }
        });
    };

    this.isRemplacant = function(values, callback){
        var param = [values.idCompte];

        connection.query('SELECT * FROM Remplacant WHERE idCompte = ?', param, function(er,row){
            if(er == null){
                    if (JSON.stringify(row) === '[]') {
                        callback(null, false);
                    } else {
                        callback(null, true);
                    }
            } else {
                callback(er);
            }
        });
    };

    this.getInformationCompte = function(values, callback){
        var that = this;
        var param = [values.email];
        if (typeof values != 'undefined' && typeof values != 'undefined') {
            connection.query('SELECT * FROM Compte WHERE email = ?', param, function(er,row){
                if(er == null){
                    var res = row[0];
                    res.mdp = "private";
                    res.cleModificationMdp = 'private';
                    that.isRemplacant(res, function (err , remplacant) {
                        if (err == null) {
                            if (remplacant) {
                                res.type = "Est un remplaçant";
                            } else{
                                res.type = "Cherche un remplaçant";
                            };
                            callback(null, res);
                        } else {
                            callback(err);
                        };
                    });
                } else {
                    callback(er);
                }
            });
        } else {
            callback("Email invalid");
        }
    };

    this.getLieuDeTravailCompte = function(values, callback){
        var that = this;
        if (typeof values != 'undefined') {
            that.getIdCompte(values, function (errrrr, rowwwww) {
                var param = [rowwwww.idCompte];
                connection.query('SELECT * FROM LieuDeTravail WHERE CompteLier = ?', param, function(er,row){
                    if(er == null){
                        var res = row[0];
                        callback(null, res);
                    } else {
                        callback(er);
                    }
                });
            });
        } else {
            callback("Email invalid");
        }
    };

    this.createLieuDeTravailCompte = function(values, callback){
        var that = this;
        if (typeof values != 'undefined') {
            that.getIdCompte(values, function (errrrr, rowwwww) {
                var param = [values.lieu.titre,values.lieu.description,0,values.lieu.informatise,values.lieu.secretariat,values.lieu.logement,values.lieu.ville,values.lieu.adresse,values.lieu.lat,values.lieu.lon,values.lieu.codePostal,values.lieu.typeStructure,rowwwww.idCompte];
                connection.query('INSERT INTO LieuDeTravail (titre,description,nbImage,informatise,secretariat,logement,ville,adresse,lat,lon,codePostal,typeStructure,CompteLier) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)', param, function(er,row){
                    if(er == null){
                        callback(null, row);
                    } else {
                        callback(er);
                    }
                });
            });
        } else {
            callback("Email invalid");
        }
    };

    this.updateCompte = function(values, callback){
        var that = this;
        var params = [values.nom, values.prenom, values.description, values.dateDeNaissance, values.idProfessionnel, values.numTel, values.specialite, values.idCompte];
        connection.query("UPDATE Compte SET nom = ?, prenom = ?, description = ?, dateDeNaissance = ?, idProfessionnel = ?, numTel = ?, specialite = ? WHERE idCompte = ?", params, function(er,row){
            if(er == null){
                callback(null, row);
            } else {
                callback(er);
            }
        });
    };

    this.updateLieuCompte = function(values, callback){
        var that = this;
        var params = [values.titre,values.description,10,values.informatise,values.secretariat,values.logement,values.ville,values.adresse,values.lat,values.lon,values.codePostal,values.typeStructure, values.CompteLier];
        connection.query("UPDATE LieuDeTravail SET titre = ?, description = ?, nbImage = ?, informatise = ?, secretariat = ?, logement = ?, ville = ?, adresse = ?, lat = ?, lon = ?, codePostal = ?, typeStructure = ? WHERE CompteLier = ?", params, function(er,row){
            if(er == null){
                callback(null, row);
            } else {
                callback(er);
            }
        });
    };

    this.createCompte = function(values, callback){
        var that = this;

        hashage(values.mdp, function(err,roww){
            if(err == null){
                var params = [values.nom, values.prenom, values.description, values.dateDeNaissance, values.idProfessionnel, values.numTel, values.email, roww, 0, values.specialite];
                connection.query('INSERT INTO Compte (nom,prenom,description,dateDeNaissance,idProfessionnel,numTel,email,mdp,actif,specialite) VALUES (?,?,?,?,?,?,?,?,?,?)', params, function(errrr, rowwww){
                    if(errrr == null){
                        that.getIdCompte(values, function (errrrr, rowwwww) {
                            if (errrrr == null) {
                                if(values.remplacant == true){
                                    connection.query('INSERT INTO Remplacant (idCompte) VALUES (?)', [rowwwww.idCompte], callback);
                                } else {
                                    connection.query('INSERT INTO Remplace (idCompte) VALUES (?)', [rowwwww.idCompte], callback);
                                }
                            } else {
                                callback(errrrr);
                            }
                        });
                    } else {
                        callback(errrr);
                    }
                });
            } else {
                callback(err);
            }
        });
    };

    this.checkMdp = function(values, callback){
    	var param = [values.email];

        hashage(values.mdp, function(err,roww){
            if(err == null){
                connection.query('SELECT mdp FROM Compte WHERE email = ?', param, function(er,row){
                    if(er == null){
                        if(JSON.stringify(row) == "[]"){
                            callback(null, {mdpBon:false, emailInexistant:true});
                        } else if (row[0].mdp == roww){
                            callback(null, {mdpBon:true, mdp:row});
                        } else {
                            callback(null, {mdpBon:false,emailInexistant:false});
                        }
                    } else {
                        callback(er);
                    }
                });
            } else {
                callback(err);
            }
        });
    };

    this.checkMdpAlreadyHashed = function(values, callback){
        var param = [values.email];

        connection.query('SELECT mdp FROM Compte WHERE email = ?', param, function(er,row){
            if(er == null){
                if(JSON.stringify(row) == "[]"){
                    callback(null, {mdpBon:false, emailInexistant:true});
                } else if (row[0].mdp == values.mdp) {
                    callback(null, {mdpBon:true, mdp:row});
                } else {
                    callback(null, {mdpBon:false,emailInexistant:false});
                }
            } else {
                callback(er);
            }
        });
    };

    this.getDateRemplacant = function(values, callback){
        var param = [values];
        connection.query('SELECT * from Annonce WHERE idCompteRemplacant = ?', param, function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    }

    this.getDate = function(values, callback){
        var param = [values];
        connection.query('SELECT * from Annonce WHERE idCompteRemplace = ?', param, function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    }

    this.addDate = function(values, callback){
        var param = [values.debut, values.fin, values.idCompteRemplace];
        connection.query('INSERT INTO Annonce (debut,fin,idCompteRemplace) Values(?,?,?)', param, function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    };

    this.updateDate = function(values, callback){
        var param1 = [values.debut , values.fin , values.idAnnonce];
        connection.query('UPDATE Annonce SET debut = ? , fin = ? WHERE idAnnonce = ? ', param1 , function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    };

    this.changeRempla = function(values, callback){
        var param1 = [values.idCompte , values.idAnnonce];
        connection.query('UPDATE Annonce SET idCompteRemplacant = ? WHERE idAnnonce = ? ', param1 , function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    };

    this.supprimerDate = function(values, callback){
        var param1 = [values.idAnnonce];
        connection.query('DELETE FROM Annonce WHERE idAnnonce = ? ', param1 , function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    };



    this.getDestination = function(values, callback){
        var param = [values];
        connection.query('SELECT * from Destination WHERE idCompte = ?', param, function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    }

    this.addDestination = function(values, callback){
        var param = [values.lat, values.lng, values.idCompte, values.rayon];
        connection.query('INSERT INTO Destination (lat,lng,idCompte,rayon) Values(?,?,?,?)', param, function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    };

    this.updateDestination = function(values, callback){
        var param = [values.lat, values.lng, values.rayon,values.idDestination];
        connection.query('UPDATE Destination SET lat = ? , lng = ?, rayon = ? WHERE idDestination = ? ', param , function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    };

    this.supprimerDestination = function(values, callback){
        var param1 = [values.idDestination];
        connection.query('DELETE FROM Destination WHERE idDestination = ? ', param1 , function(er,row){
            if(er == null){
                callback(null,row);
            } else {
                callback(er);
            }
        });
    };

    
    this.changePassword = function(values, callback)
    {
        hashage(values.body.pw, function(err, data)
        {
            if(err == null)
            {
                var params = [data, values.key, values.body.id];
                connection.query('UPDATE Compte SET mdp = ?, cleModificationMdp = ? WHERE idCompte = ?', params, callback);
            }  
            else
                callback(err);
        });
        
    };

    this.changeKey = function(values, callback)
    {
        var params = [values.key, values.id.idCompte];
        connection.query('UPDATE Compte SET cleModificationMdp = ? WHERE idCompte = ?', params, callback);
    };

    this.changeEmail = function(values, callback)
    {
        var params = [values.email, values.params.idCompte];
        connection.query('UPDATE Compte SET email = ? WHERE idCompte = ?', params, callback);
    }

    this.getKeyCompte = function(values, callback)
    {
        var param = [values.id];

        connection.query('SELECT cleModificationMdp FROM Compte WHERE idCompte = ?', param, function(er,row)
        {
            if(er == null)
                callback(null, row[0]);
            else
                callback(er);
        });
    };

    this.updateEmailCompte = function(values, callback)
    {
        var params = [values.body.email, values.email];
        connection.query('UPDATE Compte SET email = ? WHERE email = ?', params, function(err, res)
        {
            if(err == null)
                callback(null, res);
            else
                callback(err, res);
        });
    };

    this.supprimerCompte = function(id, callback)
    {
        var param = [id];
        connection.query('DELETE FROM LieuDeTravail WHERE CompteLier = ?', param, function(err)
        {
            if(err == null)
            {
                connection.query('DELETE FROM DemandeDeRemplacement WHERE idCompteRemplacant = ?', param, function(err)
                {
                    if(err == null)
                    {
                        connection.query('DELETE FROM Annonce WHERE idCompteRemplacant = ?', param, function(err)
                        {
                            if(err == null)
                            {
                                connection.query('DELETE FROM Annonce WHERE idCompteRemplace = ?', param, function(err)
                                {
                                    if(err == null)
                                    {
                                        connection.query('DELETE FROM Remplace WHERE idCompte = ?', param, function(err)
                                        {
                                            if(err == null)
                                            {
                                                connection.query('DELETE FROM Remplacant WHERE idCompte = ?', param, function(err)
                                                {
                                                    if(err == null)
                                                    {
                                                        connection.query('DELETE FROM Remplace WHERE idCompte = ?', param, function(err)
                                                        {
                                                            if(err == null)
                                                            {
                                                                connection.query('DELETE FROM Remplacant WHERE idCompte = ?', param, function(err)
                                                                {
                                                                    if(err == null)
                                                                    {
                                                                        connection.query('DELETE FROM Avis WHERE idCompte1 = ?', param, function(err)
                                                                        {
                                                                            if(err == null)
                                                                            {
                                                                                connection.query('DELETE FROM Avis WHERE idCompte2 = ?', param, function(err)
                                                                                {
                                                                                    if(err == null)
                                                                                    {
                                                                                        connection.query('DELETE FROM Signalement WHERE idCompte1 = ?', param, function(err)
                                                                                        {
                                                                                            if(err == null)
                                                                                            {
                                                                                                connection.query('DELETE FROM Signalement WHERE idCompte2 = ?', param, function(err)
                                                                                                {
                                                                                                    if(err == null)
                                                                                                    {
                                                                                                        connection.query('DELETE FROM Moderateur WHERE idCompte = ?', param, function(err)
                                                                                                        {
                                                                                                            if(err == null)
                                                                                                            {
                                                                                                                connection.query('DELETE FROM Compte WHERE idCompte = ?', param, function(err)
                                                                                                                {
                                                                                                                    if(err == null)
                                                                                                                        callback(null);
                                                                                                                    else 
                                                                                                                        callback(err);
                                                                                                                });
                                                                                                            }
                                                                                                            else
                                                                                                                callback(err);
                                                                                                        });
                                                                                                    }
                                                                                                    else 
                                                                                                        callback(err);
                                                                                                })
                                                                                            }
                                                                                            else 
                                                                                                callback(err);
                                                                                        });
                                                                                    }
                                                                                    else 
                                                                                        callback(err); 
                                                                                });
                                                                            }
                                                                            else 
                                                                                callback(err);
                                                                        });
                                                                    }
                                                                    else 
                                                                        callback(err);
                                                                });
                                                            }
                                                            else 
                                                                callback(err);
                                                        });
                                                    }
                                                    else 
                                                        callback(err);
                                                });
                                            }
                                            else 
                                                callback(err);
                                        });
                                    }
                                    else 
                                        callback(err);
                                });
                            }
                            else 
                                callback(err);
                        });
                    }
                    else 
                        callback(err);
                });
            }
            else 
                callback(err);
        });
    }

    this.addAvis = function(values, callback){

        var valeur = [values.idCompte1,values.idCompte2,values.message,values.note];
        connection.query("INSERT INTO Avis (idCompte1,  idCompte2, message ,note) Values (?,?,?,?)",valeur,function(er,row){
            if(er != null){
                callback(er);
            }
            else{
                callback(null,row);
            }
        });
    }

    this.getAvis = function(values,callback){
        connection.query('SELECT * FROM Avis,Compte WHERE idCompte2 = ? AND idCompte1 = idCompte',values.idCompte2,function(er,row){
            if(er != null){
                callback(er);
            }
            else{
                for(var i =0; i < row.length;i++){
                    row[i].mdp = 'private';
                }
                callback(null,row);
            }
        });
    }

    this.addReport = function(values, callback){
        
        var valeur = [values.idCompte1,values.idCompte2,values.message,values.note];
        connection.query("INSERT INTO Signalement (idCompte1,  idCompte2, message) Values (?,?,?)",valeur,function(er,row){
            if(er != null){
                callback(er);
            }
            else{
                callback(null,row);
            }
        });
    }

     this.getReport = function(callback){
        connection.query("SELECT * FROM Signalement",function(er,row){
            if(er != null){
                callback(er);
            }
            else{
                callback(null,row);
            }
        });
    };
};
var action_bdd = new ActionBdd();
module.exports = action_bdd;
