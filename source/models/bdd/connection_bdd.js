var mysql = require("mysql");

var connection_bdd = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database : 'mediweb'
});

connection_bdd.connect(function(err){
  if(err){
    console.log('Erreur : Probleme de connection avec la base de donnees');
  }
});

module.exports = connection_bdd;