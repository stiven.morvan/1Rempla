angular.module('app', ['ui.router','ngRoute','ngResource', 'reCAPTCHA',"xeditable",'ngMap', 'ngFileUpload', 'ui.calendar'])

//---------------
// Initialisation, configuration et directives
//---------------

.run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // Definition du théme pour les champs modifiables dans profil
})

.config(function (reCAPTCHAProvider) {
  reCAPTCHAProvider.setPublicKey('6LeExBIUAAAAALgXlC-Ttj5mrNyY07Bjz6QbeACv');
  reCAPTCHAProvider.setOptions({
      theme: 'clean'
  });
})

.config( [
    '$compileProvider',
    function( $compileProvider )
    {   
      $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel|app):/); 
    }
])

.directive('onErrorSrc', function(PhotosAnnonceInexistantesService,$timeout) {
  return {
    link: function(scope, element, attrs) {
        element.bind('error', function() {
          if (attrs.src != attrs.onErrorSrc) {

            if(!(attrs.ngSrc.match("/img/imageAnnonce/") && navigator.userAgent.toLowerCase().indexOf('firefox') > -1)){
              attrs.$set('src', attrs.onErrorSrc);
            } else {
              if (typeof attrs.iteration != "undefined") {
                attrs.iteration++;
              } else {
                attrs.iteration = 1;
              }

              if (attrs.iteration < 2) {
                attrs.$set('src', attrs.src);
              } else {
                attrs.$set('src', attrs.onErrorSrc);
              }
            }

            if (attrs.ngSrc.match("/img/imageAnnonce/") && !(attrs.ngSrc.match("none"))) {
              PhotosAnnonceInexistantesService.photos.push(attrs.ngSrc.replace('/img/imageAnnonce/','').replace('.jpg',''));
            };
          }
        });
    }
  }
})

//---------------
// Services
//---------------


.factory('PhotosAnnonceInexistantesService', function() {
  return {
      photos : []
  };
})

.factory('RemplacantService', function() {
  return {
      remplacant : false
  };
})

.factory('RechercheVilleService', function() {
  return {
      ville : null
  };
})

.factory('CompteService', function($http) {
  return {
      // Requete vers le serveur, cette requete verifiera le compte connecté et répondra en conséquence
      requeteGenerique:function(path,callback,valeur){
        var token = window.localStorage.getItem('auth');
        var ret = null;
        if(token != null){
          $http.post(path,valeur,{headers:{'x-access-token':token}}).success(function (data) {
            callback(data);
          });
        } else {
          callback(ret);
        }
      },

      getEmail:function(callback){
        this.requeteGenerique('/requete/getEmail', function(data){
          ret = null;
          if(typeof data == "undefined" || data == null){
            ret = {success:false};
          } else if(data.success == true) ret = data.res.email;
          callback(ret);
        })
      },

      getInformationCompte:function(callback){
        this.requeteGenerique('/requete/getInformationCompte', function(data){
          ret = null;
          if(typeof data == "undefined" || data == null){
            ret = {success:false};
          } else if(data.success == true) ret = data;
          callback(ret);
        })
      },
      // Pour récuperer les information publique d'un compte
      // Valeur : {idCompte: LeNumeroDeCompte}
      getInformationComptePublique:function(valeur,callback){
        this.requeteGenerique('/requete/getInformationComptePublique', function(data){
          ret = null;
          if(typeof data == "undefined" || data == null){
            ret = {success:false};
          } else if(data.success == true) ret = data;
          callback(ret);
        },valeur);
      },

      getInformationLieuPublique:function(valeur,callback){
        this.requeteGenerique('/requete/getInformationLieuPublique', function(data){
          ret = null;
          if(typeof data == "undefined" || data == null){
            ret = {success:false};
          } else if(data.success == true) ret = data;
          callback(ret);
        },valeur);
      }

  };
})

//---------------
// Controllers
//---------------


.controller('menuCtrl', function ($scope,$state, $http, $location, RechercheVilleService, CompteService) {

  $scope.isConnect = function(){
    var token = window.localStorage.getItem('auth');
    if (token != null) {
      return true;
    } else {
      return false;
    }
  };
  $scope.chargement = false;
  CompteService.getInformationCompte(function(data){
    if (data.success == true && data.res.type === "Est un remplaçant") {
      $scope.estRemplacant = true;
    } else {
      $scope.estRemplacant = false;
    }
    $scope.chargement = true;
  });

  $scope.validation = function(){
    if (typeof $scope.ville != 'undefined') {
      RechercheVilleService.ville = $scope.ville;
    };
  }
})

.controller('compteConnectionCtrl', function ($scope,$state, $http, $location) {
  $scope.erreur = false;

  if (window.localStorage.getItem('auth') != null) {
    $state.transitionTo('compteInformation');
  }

  $scope.validation = function(){
    if (!(typeof $scope.formData == 'undefined')) {
      $http.post('/login',$scope.formData).success(function (data) {
        if (data.erreur != null) {
          if (data.erreur.emailInexistant == true) {
            $scope.erreur = "Aucun compte correspond à cette adresse email.";
          } else if (data.erreur.mdpBon == false) {
            $scope.erreur = "Votre mot de passe est incorrect";
          } else {
            $scope.erreur = data.erreur;
          }
        } else {
          localStorage.setItem('auth', JSON.stringify(data));
          $state.transitionTo('compteInformation');
        }
      });
    }
  };

  $scope.erreurFormulaire = function(){
    if ($scope.erreur != false) return $scope.erreur;
    else return false;
  };

  $scope.modifierMdpSuccess = false;
  $scope.modifierMdpError = false;
  $scope.modifierMdp = function(){

    if (!(typeof $scope.formData == 'undefined')) {
      $http.post('/login',$scope.formData).success(function (data) {
        if (data.erreur != null) {
          if (data.erreur.emailInexistant == true) {
            $scope.modifierMdpError = true;
            $scope.erreur = "Aucun compte correspond à cette adresse email.";
          } else {
            $scope.modifierMdpError = false;
            $scope.modifierMdpSuccess = true;
            $http.post('/requete/email/password', $scope.formData).success(function(){});
          }
        } else {
          $scope.modifierMdpError = true;
        }
      });
    } else {
      $scope.modifierMdpError = true;
    }
  };
})

.controller('creationCompteCtrl', function ($scope, $http, $location, $state, RemplacantService) {
  $scope.choix = false;
  $scope.envoi = false;
  $scope.erreur = false;
  $scope.emailExistant = false;
  $scope.listSpe = [];

  $http.post('/requete/getSpecialite').success(function (data) {
    $scope.listSpe = data.specialite;
  });

  $scope.majeur = function(){
    if (typeof $scope.formData == 'undefined' || typeof $scope.formData.dateDeNaissance == 'undefined') {
      if ($scope.envoi) return true;
    } else {
      var limite = new Date();
      limite = limite.setFullYear(limite.getFullYear() - 18);
      naissance = Date.parse($scope.formData.dateDeNaissance);

      if(form.dateDeNaissance.$invalid || naissance > limite){
        return true;
      }
    }
    return false;
  };

  $scope.correctSpe = function(){
    var ret = false;
    if (typeof $scope.formData == 'undefined' || typeof $scope.formData.specialite == 'undefined') {
      if ($scope.envoi) ret = true;
    } else {
      ret = true;
      $scope.listSpe.forEach(function(element) {
        if(element.specialite === ""+$scope.formData.specialite) ret = false;
      });

      if(form.specialite.$invalid) ret = true;
    }
    return ret;
  }

  $scope.mdpCheck = function(){
    return ($scope.formData.mdp == $scope.formData.mdp2) ? false : true;
  };

  $scope.validation = function() {
    $scope.envoi = true;

    // Verification du formulaire
    if (typeof $scope.formData.idProfessionnel != 'undefined' && $scope.form.idProfessionnel.$valid &&
        typeof $scope.formData.dateDeNaissance != 'undefined' && $scope.form.dateDeNaissance.$valid &&
        typeof $scope.formData.prenom != 'undefined' && $scope.form.prenom.$valid &&
        typeof $scope.formData.email != 'undefined' && $scope.form.email.$valid &&
        typeof $scope.formData.mdp2 != 'undefined' && $scope.form.mdp2.$valid &&
        typeof $scope.formData.nom != 'undefined' && $scope.form.nom.$valid &&
        typeof $scope.formData.mdp != 'undefined' && $scope.form.mdp.$valid &&
        typeof $scope.formData.captcha != 'undefined' &&
        $scope.form.description.$valid &&
        $scope.form.numTel.$valid &&
        !$scope.correctSpe() &&
        !$scope.mdpCheck() &&
        !$scope.majeur()
        ) {
      $scope.formData.remplacant = RemplacantService.remplacant;
      // On envoi les info au serveur (gestion par le fichier validationCreation.js)
      $http.post('/requete/validationCreation',$scope.formData).success(function (data) {
        if (data.success == true) {
          $state.transitionTo('inscription_reussite');
        } else {
          // Gestion des erreurs renvoyer par le serveur
          if (data.erreur.code == "ER_DUP_ENTRY" && data.erreur.errno == 1062 && data.erreur.sqlState == "23000" && data.erreur.index == 0) {
            $scope.erreur = "L'adresse email que vous avez renseigné a déjà été utilisé.";
          } else if (data.erreur.code == "ER_WARN_DATA_OUT_OF_RANGE" && data.erreur.errno == 1264 && data.erreur.sqlState == "22003" && data.erreur.index == 0) {
            $scope.erreur = "++"+$scope.formData.idProfessionnel+"++";
          } else {
            $scope.erreur = JSON.stringify(data.erreur);
          }
        }
      });
    }
  };

  $scope.erreurFormulaire = function(){
    if ($scope.erreur != false) return $scope.erreur;
    else return false;
  };

  $scope.choisirRemplacant = function(){
    $state.transitionTo('choix');
    RemplacantService.remplacant = true;
  }

  $scope.choisirRemplace = function(){
    $state.transitionTo('choix');
    RemplacantService.remplacant = false;
  }

  $scope.remplace = function(){
    return !RemplacantService.remplacant;
  }
})

.controller('compteInformationCtrl', function ($scope,$state, $http, $location, RemplacantService, CompteService) {
  CompteService.getInformationCompte(function(info){
    if (info != null) {
      $scope.user = info.res;
    };
  });

  $scope.deconnection = function(){
    localStorage.removeItem('auth');
    $state.transitionTo('compte');
  };

  $scope.testConnection = function(){
    CompteService.getEmail(function(email){
      if (email != null) {
        alert(email);
      };
    });
  };

  $scope.changeEmail = function()
  {
    if(typeof $scope.user != 'undefined' && typeof $scope.user.idCompte != 'undefined')
    {
      $state.transitionTo('modificationEmail');
    }
  }

  $scope.sendEmail = function(){
    if(typeof $scope.user != 'undefined' && typeof $scope.user.idCompte != 'undefined')
    {
      $http.post('/requete/email/password', $scope.user).success(function()
      {
        $state.transitionTo('envoiEmailMdp');
      });
    }
  };

  $scope.suppression = function(){
    $state.transitionTo('suppression');
  };

})

.controller('profilPersoCtrl', function ($scope, $state, $filter, $http, $location, RemplacantService, CompteService, Upload, $window) {
  $scope.user = {};
  $scope.listSpe = new Array();

  $http.post('/requete/getSpecialite').success(function (data) {
    var u = 0;
    for(var i in data.specialite){
      $scope.listSpe.push({value: u, text: data.specialite[i].specialite});
      u++;
    }
    $scope.setInformation();
  });

  $scope.setInformation = function(){
    CompteService.getInformationCompte(function(info){
      if (info != null) {
        var naissance = new Date(info.res.dateDeNaissance);
        var jour = naissance.getDate();
        var mois = naissance.getMonth() + 1; // months are zero based
        var ans = naissance.getFullYear();
        info.res.dateDeNaissance = ans+"-"+mois+"-"+jour;
        info.res.imageDeProfil = "/img/imageDeProfil/"+info.res.idCompte+".jpg";
        $scope.user = info.res;
        $scope.$watch("user", $scope.updateCompte,true);
      };
    });
  };

  $scope.showSpecialite = function() {
    var valeur = 0;
    var u = 0;

    while(u < $scope.listSpe.length && valeur == 0){
      if (typeof $scope.user.specialite != 'undefined' && $scope.listSpe[u].text.valueOf() === $scope.user.specialite.valueOf()) valeur = u;
      u++;
    }

    var selected = $filter('filter')($scope.listSpe, {value: valeur});
    return ($scope.user.specialite && selected.length) ? selected[0].text : 'Not set';
  };

  $scope.updateCompte = function(){
    var valeur = 0;
    var u = 0;
    while(u < $scope.listSpe.length && valeur == 0){
      if ($scope.listSpe[u].value == $scope.user.specialite){
        valeur = u;
        $scope.user.specialite = $scope.listSpe[u].text;
      }
      u++;
    }

    CompteService.requeteGenerique('/requete/updateCompte', function(data){
      if(data.success == false){
        alert(JSON.stringify(data));
        $scope.setInformation();
      }
    }, $scope.user);
  };

  var vm = this;
  vm.submit = function(){
    if (vm.upload_form.file.$valid && vm.file) {
      vm.upload(vm.file);
    }
  };

  vm.upload = function (file) {
      Upload.upload({
          url: '/requete/upload/' + $scope.user.idCompte + '/imageDeProfil',
          data:{file:file}
      }).then(function (resp) {
          if(resp.data.error_code === 0){
            $window.location.reload();
          } else {
            $window.alert('an error occured');
          }
      }, function (resp) {
          $window.alert('Error status: ' + resp.status);
      }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          vm.progress = 'progress: ' + progressPercentage + '% ';
      });
  };
})

.controller('profilPubliqueCtrl', function ($scope, $state, $filter, $http, $location, $window, RemplacantService, CompteService) {
  $scope.user = {};

  var valeur = $location.search();

  if (typeof valeur.idCompte != 'undefined') {
    CompteService.getInformationComptePublique(valeur,function(info){
      if (info != null) {
        var naissance = new Date(info.res.dateDeNaissance);
        var jour = naissance.getDate();
        var mois = naissance.getMonth() + 1; // months are zero based
        var ans = naissance.getFullYear();
        info.res.dateDeNaissance = ans+"-"+mois+"-"+jour;
        info.res.imageDeProfil = "/img/imageDeProfil/"+info.res.idCompte+".jpg";
        $scope.user = info.res;
      }
    });
  }

  $scope.existe = function(){
    if (typeof $scope.user.idCompte != 'undefined') {
      return true;
    } else {
      return false;
    }
  }

  $scope.peutContacter = false;
  $scope.estRemplacant = true;
  $scope.contacter = function(){
    CompteService.getInformationCompte(function(data){
      if (data.success == true) {
        $scope.peutContacter = true;
      } else {
        $scope.estRemplacant = false;
      }
    });
  }

  $scope.veutReport = false;
  $scope.peutReport = false;
  $scope.aRepport = false;
  CompteService.getInformationCompte(function(data){
    if (data.success == true) {
      $scope.peutReport = true;
    }
  });

  $scope.repporter = function(){
    $scope.veutReport = true;
  }

  $scope.report = function(){
    var needed = {
      message: $scope.formData.text,
      idCompte2: $scope.user.idCompte
    }
    CompteService.requeteGenerique('/requete/addReport',function(data){
      $scope.aRepport = true;
    },needed);
  }
})

.controller('annoncePersoCtrl', function ($scope, $state, $filter, $http, $location, RemplacantService, $window, CompteService, NgMap, Upload, PhotosAnnonceInexistantesService) {
  PhotosAnnonceInexistantesService.photos = [];
  $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw&callback=initMap";
  $scope.lieuDeTravail;
  $scope.listStruct = new Array();
  $scope.lieuDeTravailExiste = false;
  $scope.localisationBonne = false;
  $scope.localisation = {
    latitude:0,
    longitude:0,
  }

  $http.post('/requete/getTypeStructure').success(function (data) {
    var u = 0;
    for(var i in data.specialite){
      $scope.listStruct.push({value: u, text: data.specialite[i].typeStructure});
      u++;
    }
    $scope.setInformation();
  });

  $scope.actualiserVille = function(){
    NgMap.getMap().then(function(map) {
      $http.post('https://maps.googleapis.com/maps/api/geocode/json?address='+($scope.lieuDeTravail.adresse).split(" ").join("+")+','+($scope.lieuDeTravail.ville).split(" ").join("+")+','+($scope.lieuDeTravail.codePostal)+'&components=country:FR&key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw').success(function (data) {
          if (typeof data.results[0] != 'undefined') {
            var center = data.results[0].geometry.location;
            google.maps.event.trigger(map, "resize");
            map.setCenter(new google.maps.LatLng(center.lat, center.lng));
            map.setZoom(13);
            $scope.localisation.latitude = center.lat;
            $scope.localisation.longitude = center.lng;
            $scope.localisationBonne = true;
          } else {
            map.positions = [];
            $scope.localisationBonne = false;
          }
        });
    });
  }

  $scope.showStructure = function() {
    if (typeof $scope.lieuDeTravail != 'undefined' && typeof $scope.lieuDeTravail.typeStructure != 'undefined') {
      var selected = $filter('filter')($scope.listStruct, {value: $scope.lieuDeTravail.typeStructure});
      return ($scope.lieuDeTravail.typeStructure) ? selected[0].text : 'Vous devez remplir ce champ';
    } else {
      return 'Vous devez remplir ce champ';
    }
  };

  $scope.setInformation = function(){
    CompteService.requeteGenerique('/requete/getLieuDeTravailCompte', function(data){
      if(data.success == false){
        $scope.lieuDeTravail = {
          titre:undefined,
          description:undefined,
          nbImage:undefined,
          informatise:false,
          secretariat:false,
          logement:false,
          ville:undefined,
          adresse:undefined,
          lat:0,
          lon:0,
          codePostal:0,
          typeStructure:undefined
        };
        $scope.lieuDeTravailExiste = false;
      } else {
        $scope.lieuDeTravailExiste = true;
        $scope.lieuDeTravail = data.res;

        var valeur = u = 0;
        while(u < $scope.listStruct.length && valeur == 0){
          if ($scope.listStruct[u].text === $scope.lieuDeTravail.typeStructure){
            valeur++;
            $scope.lieuDeTravail.typeStructure = $scope.listStruct[u].value;
          }
          u++;
        }

        $scope.$watch("lieuDeTravail", $scope.updateLieuDeTravail,true);
      }
      $scope.$watch("lieuDeTravail.ville", $scope.actualiserVille,true);
      $scope.$watch("lieuDeTravail.codePostal", $scope.actualiserVille,true);
      $scope.$watch("lieuDeTravail.adresse", $scope.actualiserVille,true);
    });
  };

  $scope.createLieu = function(){
    var copie = Object.assign({}, $scope.lieuDeTravail);
    (copie.informatise == false) ? (copie.informatise = 0) : (copie.informatise = 1);
    (copie.secretariat == false) ? (copie.secretariat = 0) : (copie.secretariat = 1);
    (copie.logement == false) ? (copie.logement = 0) : (copie.logement = 1);

    var valeur = u = 0;

    while(u < $scope.listStruct.length && valeur == 0){
      if ($scope.listStruct[u].value == $scope.lieuDeTravail.typeStructure){
        valeur++;
        copie.typeStructure = $scope.listStruct[u].text;
      }
      u++;
    }

    CompteService.requeteGenerique('/requete/createLieu', function(data){
      $window.location.reload();
    }, copie);
  }


  $scope.updateLieuDeTravail = function(){
    var copie = Object.assign({}, $scope.lieuDeTravail);
    (copie.informatise == false) ? (copie.informatise = 0) : (copie.informatise = 1);
    (copie.secretariat == false) ? (copie.secretariat = 0) : (copie.secretariat = 1);
    (copie.logement == false) ? (copie.logement = 0) : (copie.logement = 1);

    var valeur = u = 0;

    while(u < $scope.listStruct.length && valeur == 0){
      if ($scope.listStruct[u].value == $scope.lieuDeTravail.typeStructure){
        valeur++;
        copie.typeStructure = $scope.listStruct[u].text;
      }
      u++;
    }

    CompteService.requeteGenerique('/requete/updateLieuCompte', function(data){
      if(data.success == false){
        alert(JSON.stringify(data));
        $scope.setInformation();
      }
    }, copie);
  };

  $scope.maxImage = function(){
    if (typeof PhotosAnnonceInexistantesService.photos[0] == 'undefined') {
      return true;
    } else {
      return false;
    }
  };

  $scope.submit = function(){
    if ($scope.upload_form.file.$valid && $scope.file) {
      $scope.upload($scope.file);
    }
  };

  $scope.upload = function (file) {
    if (typeof PhotosAnnonceInexistantesService.photos[0] != 'undefined') {
      Upload.upload({
          url: '/requete/upload/' + PhotosAnnonceInexistantesService.photos[0] +'/imageAnnonce',
          data:{file:file}
      }).then(function (resp) {
          if(resp.data.error_code === 0){
            $window.location.reload();
          } else {
            $window.alert('an error occured');
          }
      }, function (resp) {
          $window.alert('Error status: ' + resp.status);
      }, function (evt) {
          var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
          $scope.progress = 'progress: ' + progressPercentage + '% ';
      });
    }
  };

  $scope.supprimerAfficher = false;
  $scope.imageSelectionner = -1;
  $scope.selectionImage = function(id){
    console.log($scope.supprimerAfficher);
    $scope.supprimerAfficher = true;
    $scope.imageSelectionner = id;
  }

  $scope.supprimerImage = function(){
    if ( $scope.imageSelectionner != -1 ) {
      CompteService.requeteGenerique('/requete/suppressionImageAnnonce', function(data){
        $window.location.reload();
      }, {
        imageSupprimer:$scope.imageSelectionner
      });
    };
  }
})


.controller('annoncePubliqueCtrl', function ($scope, $state, $filter, $http, $location, $window, $timeout, RemplacantService, CompteService, NgMap, uiCalendarConfig, $compile) {
  $scope.lieuDeTravail = {};
  $scope.localisation = {
    latitude:0,
    longitude:0,
  }
  $scope.existe = true;

  var valeur = $location.search();

  if (typeof valeur.idLieu != 'undefined') {
    CompteService.getInformationLieuPublique(valeur,function(info){
      if (typeof info != "undefined" && info != null) {
        $scope.lieuDeTravail = info.res;
        $scope.actualiserVille();
        $scope.actualiserAnnonce();
        $scope.actualiserAvis();
      } else {
        $scope.existe = false;
      }
    });
  } else {
    $scope.existe = false;
  }

  $scope.dejaCommenter = false;
  $scope.commentaireAuthoriser = false;
  CompteService.getInformationCompte(function(data){
    if (data.success == true && data.res.type != "Cherche un remplaçant") {
      $scope.commentaireAuthoriser = true;
    } else {
      console.log(data);
    }
  });

  $scope.actualiserVille = function(){
    NgMap.getMap().then(function(map) {
      $http.post('https://maps.googleapis.com/maps/api/geocode/json?address='+($scope.lieuDeTravail.adresse).split(" ").join("+")+','+($scope.lieuDeTravail.ville).split(" ").join("+")+','+($scope.lieuDeTravail.codePostal)+'&components=country:FR&key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw').success(function (data) {
          if (typeof data.results[0] != 'undefined') {
            var center = data.results[0].geometry.location;
            google.maps.event.trigger(map, "resize");
            map.setCenter(new google.maps.LatLng(center.lat, center.lng));
            map.setZoom(13);
            $scope.localisation.latitude = center.lat;
            $scope.localisation.longitude = center.lng;
          }
        });
    });
  }

  $scope.listNote = [{note:1},{note:2},{note:3},{note:4},{note:5}];

  $scope.actualiserAvis = function(){
      $scope.needed = {
        idCompte2 : $scope.lieuDeTravail.idCompte
      };
      CompteService.requeteGenerique('/requete/getAvis',function(data){
          if (data.success == true) {
            $scope.commentaires = data.res;
            if(data.res.length != 0){
              var moyenne = 0;
              var i = 0;
              while(i<data.res.length){
                moyenne += data.res[i].note;
                i++;
              };
              moyenne /=  i;
            };
          } else {
            console.log(data);
          }
      },$scope.needed);
  };

  $scope.validation = function(){
    var needed = {
      message: $scope.formData.text,
      note: $scope.formData.note,                    // rentré en dur ajouté input
      idCompte2: $scope.lieuDeTravail.idCompte
    };
    CompteService.requeteGenerique('/requete/addAvis',function(data){
      if (data == true) {
        $window.location.reload();
      } else {
        $scope.dejaCommenter = true;
      }
    },needed);
  };

  $scope.eventSources = [];
  $scope.events = [];

  $scope.formatDate = function(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  // REFRESH
  $scope.actualiserAnnonce = function(){
    if (typeof $scope.lieuDeTravail.idCompte != "undefined") {
      CompteService.requeteGenerique('/requete/getCalandarPublique', function (data) {
        if (data.success == true) {
          $scope.uiConfig.calendar.events = [];

          for(var i in data.res){
            var tempS = new Date(data.res[i].debut);
            var tempE = new Date(data.res[i].fin);

            data.res[i].start = $scope.formatDate(tempS);
            data.res[i].end = $scope.formatDate(tempE);
            if (data.res[i].idCompteRemplacant != null) {
              data.res[i].title = "Remplaçé";
              data.res[i].className = ['estRemplace'];
            } else {
              data.res[i].title = "Besoin d'un remplaçant";
              data.res[i].className = ['chercheRemplace'];
            }
            $scope.uiConfig.calendar.events.push(data.res[i]);
          }
        } else {
          alert(data);
        }
      }, {idCompte:$scope.lieuDeTravail.idCompte});
    };
  };

  // CONFIGURATION
  $scope.uiConfig = {
    calendar:{
      editable: false,
      lang: 'de',
      header:{
        left: 'title',
        center: '',
        right: 'month prev,next'
      },
      events: $scope.events
    }
  };
})

















.controller('rechercheCtrl', function ($scope, $state, $filter, $http, $location, $timeout, RemplacantService, CompteService, NgMap, RechercheVilleService) {
  $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw&callback=initMap";

  NgMap.getMap().then(function(map) {
    google.maps.event.addListener(map, 'idle', function() {
      $scope.changeVisibleMarkeur();
    });
  });

  $scope.positions = [];

  $http.post('/requete/getAnnonces').success(function (data) {
    var annonce;
    var i = -1;
    $scope.recursiveAnnonce(i,data);
  });

  $scope.listSpe = [];

  $http.post('/requete/getSpecialite').success(function (data) {
    $scope.listSpe = data.specialite;
  });

  $scope.recursiveAnnonce = function(i,data){
    i++;

    if (i < data.annonces.length) {
      var annonce = data.annonces[i];
      $http.post('https://maps.googleapis.com/maps/api/geocode/json?address='+(annonce.adresse).split(" ").join("+")+','+(annonce.ville).split(" ").join("+")+','+(annonce.codePostal)+'&components=country:FR&key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw').success(function (local) {
        if (typeof local.results[0] != 'undefined') {
          var temp = {
            id:annonce.idLieu,
            data:annonce,
            visible: true,
            pos:[local.results[0].geometry.location.lat,local.results[0].geometry.location.lng]
          };
          $scope.positions.push(temp);
        }
        $scope.recursiveAnnonce(i,data);
      });
    }
  }

  $scope.actualiserVille = function(){
    NgMap.getMap().then(function(map) {
      if (RechercheVilleService.ville != null) {
        $http.post('https://maps.googleapis.com/maps/api/geocode/json?address='+RechercheVilleService.ville+'&components=country:FR&key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw').success(function (data) {
          var center = data.results[0].geometry.location;
          google.maps.event.trigger(map, "resize");
          map.setCenter(new google.maps.LatLng(center.lat, center.lng));
          map.setZoom(13);
        });
      }
    });
  }

  $scope.$watch(function() { return RechercheVilleService.ville; }, function(newVal){
    $scope.actualiserVille();
  }, true);

  $scope.class = "tailleInvisible";
  var annonceSelectionner = -1;

  /**
   * Est executer lorsqu'on clique sur un marqueur
   */
  $scope.selectionElement = function(var1,id){
    var y = 0;
    while(typeof $scope.positions[y] != 'undefined' && $scope.positions[y].id != id){
      y++;
    }

    if(typeof $scope.positions[y] != 'undefined') {
      annonceSelectionner =  $scope.positions[y].id;
      $timeout(function(){
        $scope.class = "tailleNormale";
      },500);
    }
  }

  $scope.estSelectionner = function(id){
    if (annonceSelectionner == id) {
      return true;
    } else {
      return false;
    }
  }

  $scope.changeVisibleMarkeur = function(marker){
    NgMap.getMap().then(function(map){
      var afficher = [];
      var markers = map.markers;
      if (typeof markers != 'undefined') {
        for (var i=0; typeof markers[i] != 'undefined'; i++){
          if( map.getBounds().contains(markers[i].getPosition()) ){
            afficher.push(markers[i]);
          }
        }

        var u = 0;
        while(typeof $scope.positions[u] != 'undefined'){
          var y = 0;
          while(typeof afficher[y] != 'undefined' && ($scope.positions[u].pos[0].toFixed(2) != afficher[y].getPosition().lat().toFixed(2) || $scope.positions[u].pos[1].toFixed(2) != afficher[y].getPosition().lng().toFixed(2))){
            y++;
          }

          if (typeof afficher[y] != 'undefined') {
            $scope.positions[u].visible = true;
          } else {
            $scope.positions[u].visible = false;
          }

          u++;
        }
      }

    });
  }

  $scope.estVisible = function(id){
    var i = -1;
    do{
      i++;
    } while(typeof $scope.positions[i] != 'undefined' && $scope.positions[i].id != id);

    if (typeof $scope.positions[i] != 'undefined') {
      if (typeof $scope.positions[i].visibleInterne != "undefined") {
        return $scope.positions[i].visible && $scope.positions[i].visibleInterne;
      } else {
        return $scope.positions[i].visible;
      }
    } else {
      return false;
    }
  }

  $scope.setVisibleInterne = function(id,vision){
    var i = -1;
    do{
      i++;
    } while(typeof $scope.positions[i] != 'undefined' && $scope.positions[i].id != id);

    if (typeof $scope.positions[i] != 'undefined') {
      $scope.positions[i].visibleInterne = vision;
    }
  }

  $scope.filtre = {
    secretariat:false,
    informatise:false,
    logement:false
  };


  $scope.filtresUpdate = function(){NgMap.getMap().then(function(map) {
      var i = 0;

      while(typeof map.markers[i] != 'undefined'){
        var dateReg = /(\d{4})-(\d{2})-(\d{2})/;

        // Exemple de démonstration
        if ((map.markers[i].data.data.secretariat == 0) && ($scope.filtre.secretariat == true)) {
            map.markers[i].setVisible(false);
            $scope.setVisibleInterne(map.markers[i].data.data.idLieu,false);
        } else if((map.markers[i].data.data.informatise == 0) && ($scope.filtre.informatise == true)){
            map.markers[i].setVisible(false);
            $scope.setVisibleInterne(map.markers[i].data.data.idLieu,false);
        } else if((map.markers[i].data.data.logement == 0) && ($scope.filtre.logement == true)){
            map.markers[i].setVisible(false);
            $scope.setVisibleInterne(map.markers[i].data.data.idLieu,false);
        } else if(typeof $scope.filtre.specialite != "undefined" && !(map.markers[i].data.data.specialite === $scope.filtre.specialite.specialite)){
            map.markers[i].setVisible(false);
            $scope.setVisibleInterne(map.markers[i].data.data.idLieu,false);
        } else {
            map.markers[i].setVisible(true);
            $scope.setVisibleInterne(map.markers[i].data.data.idLieu,true);
        };

        if (($scope.filtre.fin != null) && ($scope.filtre.fin.match(dateReg))){
          var test = false;
          for(var j = 0 ; j < map.markers[i].data.data.annonces.length;j++ ){
            var deb = new Date(map.markers[i].data.data.annonces[j].fin);
            var deb2 = new Date($scope.filtre.fin);
            if(deb.getTime() >= deb2.getTime()){
              test = true;
            };
          };
          if(test){
            map.markers[i].setVisible(false);
            $scope.setVisibleInterne(map.markers[i].data.data.idLieu,false);
          };
        }

        if (($scope.filtre.debut != null) && ($scope.filtre.debut.match(dateReg))){
          var test = false;
          for(var j = 0 ; j < map.markers[i].data.data.annonces.length;j++ ){
            var deb = new Date(map.markers[i].data.data.annonces[j].debut);
            var deb2 = new Date($scope.filtre.debut);
            if(deb.getTime() <= deb2.getTime()){
              test = true;
            };
          };
          if(test){
            map.markers[i].setVisible(false);
            $scope.setVisibleInterne(map.markers[i].data.data.idLieu,false);
          };
        } 

        i++;
      }
    });
  }
  $scope.$watch("filtre", $scope.filtresUpdate,true);
})


.controller('CalandarCtrl', function ($scope,$state, $http, $location,uiCalendarConfig, CompteService, $compile, $timeout,$sce) {
  $scope.eventSources = [];
  $scope.events = [];

  $scope.formatDate = function(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  // REFRESH
  $scope.actualiserAnnonce = function(){
    CompteService.requeteGenerique('/requete/getCalandar', function (data) {
      if (data.success == true) {
        $scope.uiConfig.calendar.events = [];

        for(var i in data.res){

          var tempS = new Date(data.res[i].debut);
          var tempE = new Date(data.res[i].fin);
          data.res[i].start = $scope.formatDate(tempS);
          data.res[i].end = $scope.formatDate(tempE);

          if (data.res[i].idCompteRemplacant != null) {
            data.res[i].title = "Remplaçé";
            data.res[i].className = ['estRemplace'];
          } else {
            data.res[i].title = "Besoin d'un remplaçant";
            data.res[i].className = ['chercheRemplace'];
          }

          $scope.uiConfig.calendar.events.push(data.res[i]);
        }
      } else {
        console.log(data);
      }
    }, null);
  };
  $scope.actualiserAnnonce();

  // CREATION
  $scope.erreurAddDate = false;
  $scope.addDate = function() {
    $scope.evenement = {
      titre:"Besoin d'un remplaçant",
      debut:$scope.periode.debut,
      fin:$scope.periode.fin
    }
    CompteService.requeteGenerique('/requete/addCalandar', function (data) {
      if (data.success == true) {
        $scope.actualiserAnnonce();
        $scope.erreurAddDate = false;
      } else {
        $scope.erreurAddDate = true;
      }
    }, $scope.evenement);
  };

  $scope.extraEventSignature = function(event) {
     return "" + event;
  };

  // UPDATE
  $scope.updateDate = function(event){
    var valeur = {};
    if (event.end != null) {
      valeur.fin = event.end._d.toISOString().substring(0, 10);
    } else {
      valeur.fin = event.start._d.toISOString().substring(0, 10);
    }
    valeur.debut = event.start._d.toISOString().substring(0, 10);
    valeur.idAnnonce = event.idAnnonce;

    CompteService.requeteGenerique('/requete/updateCalandar', function (data) {
      if (data.success == true) {
        $scope.actualiserAnnonce();
      } else {
        console.log(data);
      }
    }, valeur);
  }

  // SUPPRESSION
  $scope.evenementSelectionner = false;
  $scope.evenementDebut = false;
  $scope.evenementFin = false;
  $scope.alertOnEventClick = function( date, jsEvent, view){
      $scope.evenementSelectionner = date;
      $scope.evenementDebut = $scope.formatDate(new Date(date.debut));
      $scope.evenementFin = $scope.formatDate(new Date(date.fin));
      $scope.remplacantActuel(date);
      if ($scope.evenementSelectionner.idCompteRemplacant != null) {
        CompteService.getInformationComptePublique({idCompte:$scope.evenementSelectionner.idCompteRemplacant},function(info){
          if (info != null) {
            $scope.remplacantActuelString = info.res;
          }
        });
      }
  };

  $scope.supprimerEvenement = function(){
    if ($scope.evenementSelectionner != false) {
      CompteService.requeteGenerique('/requete/supprimerDate', function (data) {
        if (data.success == true) {
          $scope.actualiserAnnonce();
          $scope.evenementSelectionner = false;
        } else {
          console.log(data);
        }
      }, {
        idAnnonce:$scope.evenementSelectionner.idAnnonce
      });

    }
  }

  $scope.remplacantAbsentString = "Aucun remplaçant n'est attibuer à cette période. Vous pouvez en attribuer un en insérant et en validant son adresse email dans le formulaire suivant.";
  $scope.remplacantActuel = function() {
    if ($scope.evenementSelectionner != false) {
      if ($scope.evenementSelectionner.idCompteRemplacant != null) {
        $scope.remplacantAbsentString = "";
        return true;
      } else {
        $scope.remplacantAbsentString = "Aucun remplaçant n'est attibuer à cette période. Vous pouvez en attribuer un en insérant et en validant son adresse email dans le formulaire suivant.";
        return false;
      }
    }
  };

  //ATTRIBUTION à Un REMPLACANT
  $scope.erreurChangeRempla = false;
  $scope.changeRempla = function(){
    if ($scope.evenementSelectionner != false) {
      CompteService.requeteGenerique('/requete/changeRemplaDate', function (data) {
        if (data.success == true) {
          $scope.actualiserAnnonce();
          $scope.evenementSelectionner = false;
          $scope.erreurChangeRempla = false;
        } else {
          $scope.erreurChangeRempla = true;
        }
      }, {
        idAnnonce:$scope.evenementSelectionner.idAnnonce,
        email:$scope.periode.email
      });
    }
  }


  // CONFIGURATION
  $scope.uiConfig = {
    calendar:{
      height: "100%",
      width: "100%",
      editable: true,
      lang: 'de',
      header:{
        left: 'title',
        center: '',
        right: 'month prev,next'
      },
      events: $scope.events,
      eventClick: $scope.alertOnEventClick,
      eventDrop: function(event, delta, revertFunc) {
        if (!confirm("Voulez vous vraiment déplacer cette période ?")) {
            revertFunc();
        } else{
          $scope.updateDate(event);
        }
      },
      eventResize: function(event, delta, revertFunc) {
        if (!confirm("Voulez vous vraiment modifier cette période ?")) {
          revertFunc();
        } else{
          $scope.updateDate(event);
        }
      }
    }
  };

  $scope.titre = "2017";
  $scope.modificationTitre = function(){
    var h2 = document.querySelectorAll(".fc-left h2");
    if (typeof h2[0] != "undefined") {
      var r = h2[0].innerHTML;
      r = r.replace("undefined","");
      r = r.replace("undefined","");
      $scope.titre = r;
    }

    $timeout(function() {
      $scope.modificationTitre();
    }, 150);
  }
  $scope.modificationTitre();
})

.controller('CalandarRemplacantCtrl', function ($scope,$state, $http, $location,uiCalendarConfig, CompteService, $compile, $timeout,$sce) {
  $scope.eventSources = [];
  $scope.events = [];

  $scope.formatDate = function(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [year, month, day].join('-');
  }

  // REFRESH
  $scope.actualiserAnnonce = function(){
    CompteService.requeteGenerique('/requete/getCalandarRemplacant', function (data) {
      if (data.success == true) {
        $scope.uiConfig.calendar.events = [];
        for(var i in data.res){
          var tempS = new Date(data.res[i].debut);
          var tempE = new Date(data.res[i].fin);
          data.res[i].start = $scope.formatDate(tempS);
          data.res[i].end = $scope.formatDate(tempE);
          data.res[i].title = "Remplaçant";
          data.res[i].className = ['estRemplace'];
          $scope.uiConfig.calendar.events.push(data.res[i]);
        }
      }
    }, null);
  };
  $scope.actualiserAnnonce();

  // SUPPRESSION
  $scope.evenementSelectionner = false;
  $scope.evenementDebut = false;
  $scope.evenementFin = false;
  $scope.alertOnEventClick = function( date, jsEvent, view){
      $scope.evenementSelectionner = date;
      $scope.evenementDebut = $scope.formatDate(new Date(date.debut));
      $scope.evenementFin = $scope.formatDate(new Date(date.fin));
      if ($scope.evenementSelectionner.idCompteRemplace != null) {
        CompteService.getInformationComptePublique({idCompte:$scope.evenementSelectionner.idCompteRemplace},function(info){
          if (info != null) {
            $scope.remplacantActuelString = info.res;
          }
        });
      }
  };

  // CONFIGURATION
  $scope.uiConfig = {
    calendar:{
      height: "100%",
      width: "100%",
      editable: false,
      lang: 'de',
      header:{
        left: 'title',
        center: '',
        right: 'month prev,next'
      },
      events: $scope.events,
      eventClick: $scope.alertOnEventClick
    }
  };

  $scope.titre = "2017";
  $scope.modificationTitre = function(){
    var h2 = document.querySelectorAll(".fc-left h2");
    if (typeof h2[0] != "undefined") {
      var r = h2[0].innerHTML;
      r = r.replace("undefined","");
      r = r.replace("undefined","");
      $scope.titre = r;
    }
    $timeout(function() {
      $scope.modificationTitre();
    }, 150);
  }
  $scope.modificationTitre();
})


.controller('potentielRemplacantCtrl', function ($scope, $state, $filter, $http, $location, $timeout, RemplacantService, CompteService, NgMap, RechercheVilleService) {

  $scope.listRempla = [];

  $http.post('/requete/getListRempla').success(function (data) {
    $scope.listRempla = data.annonces;
  });
})



.controller('preferenceRemplacantCtrl', function ($scope, $state, $filter, $http, $location, $timeout, RemplacantService, CompteService, NgMap, RechercheVilleService) {
  $scope.googleMapsUrl="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw&callback=initMap";
  $scope.destinations = [];

  $scope.erreurVille = false;
  $scope.addDestiantion = function(){
    var valeur = {
      ville:$scope.dest.ville,
      rayon:4000
    };

    $http.post('https://maps.googleapis.com/maps/api/geocode/json?address='+valeur.ville+'&components=country:FR&key=AIzaSyCN34LOjMDUlj4A8acTvUJwN9eMBPc_NJw').success(function (carte) {
      if (typeof carte.results[0] != 'undefined') {
        $scope.erreurVille = false;
        valeur.lat = carte.results[0].geometry.location.lat;
        valeur.lng = carte.results[0].geometry.location.lng;
        CompteService.requeteGenerique('/requete/addDestination', function (data) {
          if (data.success == true) {
            $scope.actualiserMap();
          }
        }, valeur);
      } else {
        $scope.erreurVille = true;
      }
    });
  };

  $scope.actualiserMap = function(){
    CompteService.requeteGenerique('/requete/getDestination', function (data) {
      if (data.success == true) {
        for (var i = data.res.length - 1; i >= 0; i--) {
          data.res[i].location = [data.res[i].lat,data.res[i].lng];
        };
        $scope.destinations = data.res;
      } else {
        console.log(data);
      }
    }, null);
  }
  $scope.actualiserMap();

  $scope.circleChange = function(param,paramCercle){

    if (typeof paramCercle != "undefined") {
      NgMap.getMap().then(function(map){
        var cercles = map.shapes;
        if (typeof cercles[paramCercle.idDestination] != "undefined") {
          var temp = cercles[paramCercle.idDestination];
          var cercle = null;
          var u = 0;

          while(u < $scope.destinations.length && cercle == null){
            if ($scope.destinations[u].idDestination == paramCercle.idDestination) {
              cercle = $scope.destinations[u];
            }
            u++;
          }

          if (cercle != null) {
            if (cercle.rayon != temp.radius || cercle.lat != temp.getCenter().lat() || cercle.lng != temp.getCenter().lng()) {
              //console.log(cercle.idDestination);
              cercle.rayon = temp.radius;
              cercle.lat = temp.getCenter().lat();
              cercle.lng = temp.getCenter().lng();
              CompteService.requeteGenerique('/requete/updateDestination', function (data) {}, cercle);
            };
          };
        };
      });
    };
  };

  $scope.supprimerDest = function(dest){
    if (typeof dest != "undefined") {
      CompteService.requeteGenerique('/requete/supprimerDestination', function (data) {
        $scope.actualiserMap();
      }, {idDestination:dest});
    };
  }
})

.controller('modificationMdpCtrl', function($scope, $state, $http, $location)
{
  var user = $location.search();

  $scope.mdpCheck = function()
  {
    return ($scope.pw1 == $scope.pw2) ? false : true;
  };

  $scope.valider = function()
  {
    if(!$scope.mdpCheck() && $scope.pw2 != undefined && $scope.pw1 != undefined)
    {     
      $http.post('/requete/modificationMdp/', {id: user.idCompte, pw: $scope.pw2, key: user.key}).success(function()
      {
        localStorage.removeItem('auth');
        $state.transitionTo('sendEmail');
      });
    }
  };
})
.controller('modificationEmailCtrl', function($scope, $state, CompteService)
{
  $scope.valider = function()
  {
    if($scope.email != '' && typeof $scope.email != 'undefined')
    {
      CompteService.requeteGenerique('/requete/modificationEmail', function(data)
      {
        if(data.success == true)
        {
          localStorage.removeItem('auth');
          $state.transitionTo('sendEmail');

        }
        else alert(JSON.stringify(data));
      }, {email: $scope.email});
    }
  };
})
.controller('suppressionCtrl', function($scope, $state, $http, CompteService)
{
  
  $scope.supprimer = function()
  {
    CompteService.requeteGenerique('/requete/suppressionCompte', function(data)
    {
      if(data.success == false)
        alert(JSON.stringify(data));
      else
      {
        localStorage.removeItem('auth');
        $state.transitionTo('sendEmail');
      }
    }, null);
  };

  $scope.annuler = function()
  {
    $state.transitionTo('compte');
  };
})


.controller('adminCtrl', function ($scope, $state, $http, $window, CompteService ) {
  CompteService.requeteGenerique('/requete/getReport',function(data){
    if (data.success == true) {
      $scope.commentaires = data.res;
    } else {
      console.log(data);
    }
  });

  $scope.supr = function(item){
    var valeur = {idCompte:item};
    CompteService.requeteGenerique('/requete/suppressionAdmin', function(data) {
      if(data.success == false) alert(JSON.stringify(data));
      $window.location.reload();
    }, valeur);
  };
});