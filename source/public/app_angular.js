angular.module('app')

.config(["$locationProvider", function($locationProvider) {
  $locationProvider.html5Mode(true);
}])

.config(
  function ($stateProvider, $urlRouterProvider) {
    $stateProvider

    .state('index', {
      url:'/',
      views:{
        'main':{ 
          templateUrl: 'template/index.html',
          controller: "creationCompteCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('formCreation', {
      url:'/formCreation',
      views:{
        'main':{ 
          templateUrl: 'template/type.html',
          controller: "creationCompteCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('choix', {
      url:'/formCreation/choix',
      views:{
        'main':{ 
          templateUrl: 'template/choix.html',
          controller: "creationCompteCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('inscription_reussite', {
      views:{
        'main':{ 
          templateUrl: 'template/inscription_reussite.html',
          controller: "creationCompteCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('compte', {
      url:'/compte',
      views:{
        'main':{ 
          templateUrl: 'template/compte.html',
          controller: "compteConnectionCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('profilPerso', {
      url:'/profilPerso',
      views:{
        'main':{ 
          templateUrl: 'template/profilPerso.html',
          controller: "profilPersoCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('profil', {
      url:'/profil',
      views:{
        'main':{ 
          templateUrl: 'template/profil.html',
          controller: "profilPubliqueCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('compteInformation', {
      url:'/compteInformation',
      views:{
        'main':{ 
          templateUrl: 'template/compteInformation.html',
          controller: "compteInformationCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('annoncePerso', {
      url:'/annoncePerso',
      views:{
        'main':{ 
          templateUrl: 'template/annoncePerso.html',
          controller: "annoncePersoCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('annonce', {
      url:'/annonce',
      views:{
        'main':{ 
          templateUrl: 'template/annonce.html',
          controller: "annoncePubliqueCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('calendrier', {
      url:'/calendrier',
      views:{
        'main':{ 
          templateUrl: 'template/calandarRemplace.html',
          controller: "CalandarCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('calendrierRemplacant', {
      url:'/calendrierRemplacant',
      views:{
        'main':{ 
          templateUrl: 'template/calandarRemplacant.html',
          controller: "CalandarRemplacantCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('preferenceRemplacant', {
      url:'/preferenceRemplacant',
      views:{
        'main':{ 
          templateUrl: 'template/preferenceRemplacant.html',
          controller: "preferenceRemplacantCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })
    
    .state('potentielRemplacant', {
      url:'/potentielRemplacant',
      views:{
        'main':{ 
          templateUrl: 'template/potentielRemplacant.html',
          controller: "potentielRemplacantCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('recherche', {
      url:'/recherche',
      views:{
        'main':{ 
          templateUrl: 'template/recherche.html',
          controller: "rechercheCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('modificationMdp',
      {
        url:'/modificationMdp',
        views:
        {
          'main':
          {
            templateUrl: 'template/modificationMdp.html',
            controller: 'modificationMdpCtrl'
          },
          'menu':
          {
          templateUrl : 'template/menu.html',
          controller: 'menuCtrl'
          }
        }
      })

    .state('envoiEmailMdp', {
      url:'/envoiEmailMdp',
      views:{
        'main':{ 
          templateUrl: 'template/envoiEmailMdp.html',
          controller: "creationCompteCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })

    .state('sendEmail', {
      url:'/sendEmail',
      views:{
        'main':{ 
          templateUrl: 'template/modification_reussite.html',
          controller: "creationCompteCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })
    
    .state('modificationEmail',
      {
        url:'/modificationEmail',
        views:
        {
          'main':
          {
            templateUrl: 'template/modificationEmail.html',
            controller: 'modificationEmailCtrl'
          },
          'menu':
          {
          templateUrl : 'template/menu.html',
          controller: 'menuCtrl'
          }
        }
      })

    .state('admin', {
      url:'/admin',
      views:{
        'main':{ 
          templateUrl: 'template/admin.html',
          controller: "adminCtrl"
        },
        'menu':{
          templateUrl : 'template/menu.html',
          controller: "menuCtrl"
        }
      }
    })
    
    .state('suppression',
      {
        url:'/suppression',
        views:
        {
          'main':
          {
            templateUrl: 'template/suppressionCompte.html',
            controller: 'suppressionCtrl'
          },
          'menu':
          {
          templateUrl : 'template/menu.html',
          controller: 'menuCtrl'
          }
        }
      });
  });