var express = require('express');
var router = express.Router();
var bdd = require('../models/bdd/action_bdd');
var jwtauth = require('./jwtauth.js');
var XMLHttpRequest = require("node-XMLHttpRequest-master").XMLHttpRequest;
var multer  = require('multer');
var nodemailer = require('nodemailer');
var htmlToText = require('html-to-text');
var path = require('path');
var fs = require('fs');

router.post('/', function(req, res, next) {
	res.send(null);
});

router.post('/getSpecialite', function(req, res, next) {
	bdd.listSpecialite(function(err, rows) {
	   	if(err != null){
			console.log("ERROR= " +err);
		} else {
			res.send({ specialite: rows });
		}
	})
});

router.post('/getListRempla', function(req, res, next) {
    bdd.getListRempla(function(err, rows){
        if(err != null){
            console.log("ERROR= " +err);
        } else {
            res.send({ annonces: rows });
        }
    })
});

router.post('/getAnnonces', function(req, res, next) {
    bdd.getAnnonces(function(err, rows){
        if(err != null){
            console.log("ERROR= " +err);
        } else {
            res.send({ annonces: rows });
        }
    })
});

router.post('/getTypeStructure', function(req, res, next) {
    bdd.listTypeStructure(function(err, rows) {
        if(err != null){
            console.log("ERROR= " +err);
        } else {
            res.send({ specialite: rows });
        }
    })
});

router.post('/getEmail', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
		if(erreur != null){
			res.send({ success: false, error:erreur});
		} else {
			res.send({ success: true, res:resultat});
		}
    })
});

router.post('/updateCompte', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            // on verifi que le compte a modifier est le même que celuit qui est connecter
            if(resultat.email === req.body.email){
                bdd.updateCompte(req.body,function(err, resultatCompte){
                    if(err != null){
                        res.send({ success: false, error:err});
                    } else {
                        res.send({ success: true, res:resultatCompte});                
                    }
                });
            } else {
                res.send({ success: false, error:"Email invalide"});
            }

        }
    })
});

router.post('/updateLieuCompte', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {

            bdd.updateLieuCompte(req.body,function(err, resultatCompte){
                if(err != null){
                    res.send({ success: false, error:err});
                } else {
                    res.send({ success: true, res:resultatCompte});                
                }
            });

        }
    })
});

router.post('/getInformationCompte', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getInformationCompte(resultat,function(err, resultatCompte){
                if(err != null){
                    res.send({ success: false, error:err});
                } else {
                    res.send({ success: true, res:resultatCompte});                
                }
            });
        }
    })
});

router.post('/getLieuDeTravailCompte', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getLieuDeTravailCompte(resultat,function(err, resultatCompte){
                if(err != null || typeof resultatCompte == 'undefined'){
                    res.send({ success: false, error:err});
                } else {
                    res.send({ success: true, res:resultatCompte});                
                }
            });
        }
    })
});

router.post('/getInformationComptePublique', function(req, res, next) {
    var valeur = req.body;
    bdd.getEmail(valeur, function (er, row) {
        if (er != null) {
            res.send({ success: false, error:er});
        } else {
            if (typeof row == 'undefined' || JSON.stringify(row) == "[]") {
                res.send({ success: false, error:"Compte inexistant"});
            } else {
                bdd.getInformationCompte(row,function(err, resultatCompte){
                    if(err != null){
                        res.send({ success: false, error:err});
                    } else {
                        res.send({ success: true, res:resultatCompte});                
                    }
                });
            }
        }
    });
});


router.post('/getInformationLieuPublique', function(req, res, next) {
    var valeur = req.body;
    if (typeof valeur != "undefined" && typeof valeur.idLieu != "undefined") {
        bdd.getInformationLieu(valeur,function(err, resultatCompte){
            if(err != null){
                res.send({ success: false, error:err});
            } else {
                res.send({ success: true, res:resultatCompte});                
            }
        });
    } else {
        res.send({ success: false, error:"idLieu undefined"});
    }
});

router.post('/createLieu', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            resultat.lieu = req.body;
            bdd.createLieuDeTravailCompte(resultat,function(err, resultatCompte){
                if(err != null){
                    res.send({ success: false, error:err});
                } else {
                    res.send({ success: true, res:resultatCompte});                
                }
            });
        }
    })
});

router.post('/validationCreation', function(req, res, next) {

    //Script qui devrais verifier le captcha
    // var http = new XMLHttpRequest();
    // var params = "?response="+encodeURIComponent(req.body.captcha.response)+"&secret="+encodeURIComponent("6LeExBIUAAAAAPbiDANB3zlFC6jLCBEK1XBGwlMo");
    // params = params.replace(/%20/g, '+');
    // http.open("POST", "https://www.google.com/recaptcha/api/siteverify"+params, true);
    // http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    // http.onreadystatechange = function() {
    //     //console.log(http.responseText);
    // }
    // http.send();
    // A modifier (au dessus)
    

    //on créer le compte
    bdd.createCompte(req.body,function(err, rows) {
        // S'il y a une erreur on la renvoie
        if(err != null){
            res.send({success: false, erreur: err});
        } else{
            res.send({success: true});
        }
    });
});

router.post('/getCalandarPublique', function(req, res, next) {
    if (typeof req.body.idCompte != "undefined") {
        bdd.getDate(req.body.idCompte,function(e,ress){
            if(e != null){
                res.send({success: false,error:e});
            } else {
                res.send({success: true,res:ress});
            }
        });
    } else {
        res.send({success: false,error:"Parametres incorrectes"});
    }
});

router.post('/getCalandar', function(req, res, next) {

    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    bdd.isRemplacant(rows.idCompte,function(er,resu){
                        if(resu == true){
                            console.log("ERROR= " +err);
                        } else {
                            bdd.getDate(rows.idCompte,function(e,ress){
                                if(e != null){
                                    res.send({success: false,error:e});
                                } else {
                                    res.send({success: true,res:ress});
                                }
                            });
                        }
                    });
                }
            });
        }
    });
});

router.post('/getCalandarRemplacant', function(req, res, next) {

    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    bdd.getDateRemplacant(rows.idCompte,function(e,ress){
                        if(e != null){
                            res.send({success: false,error:e});
                        } else {
                            res.send({success: true,res:ress});
                        }
                    });
                }
            });
        }
    });
});

router.post('/addCalandar', function(req, res, next) {

    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    var valeur = req.body;
                    valeur.idCompteRemplace = rows.idCompte;
                    bdd.addDate(valeur,function(err, rows) {
                        if(err != null){
                            res.send({success: false, error:err});
                        } else {
                            res.send({success: true});
                        }
                    });
                }
            });
        }
    });
});

router.post('/updateCalandar', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    var valeur = req.body;
                    bdd.updateDate(valeur,function(err, rows) {
                        if(err != null){
                             res.send({success: false,error:err});
                        } else {
                            res.send({success: true});
                        }
                    });
                }
            });
        }
    });
});

router.post('/supprimerDate', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    var valeur = req.body;
                    bdd.supprimerDate(valeur,function(err, rows) {
                        if(err != null){
                             res.send({success: false,error:err});
                        } else {
                            res.send({success: true});
                        }
                    });
                }
            });
        }
    });
});

router.post('/changeRemplaDate', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(req.body,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    var valeur = req.body;
                    valeur.idCompte = rows.idCompte;
                    bdd.changeRempla(valeur,function(err, rows) {
                        if(err != null){
                             res.send({success: false,error:err});
                        } else {
                            res.send({success: true});
                        }
                    });
                }
            });
        }
    });
});


router.post('/getDestination', function(req, res, next) {

    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    bdd.getDestination(rows.idCompte,function(e,ress){
                        if(e != null){
                            res.send({success: false,error:e});
                        } else {
                            res.send({success: true,res:ress});
                        }
                    });
                }
            });
        }
    });
});

router.post('/addDestination', function(req, res, next) {

    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    var valeur = req.body;
                    valeur.idCompte = rows.idCompte;
                    bdd.addDestination(valeur,function(err, rows) {
                        if(err != null){
                            res.send({success: false, error:err});
                        } else {
                            res.send({success: true});
                        }
                    });
                }
            });
        }
    });
});

router.post('/updateDestination', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    var valeur = req.body;
                    valeur.idCompte = rows.idCompte;
                    bdd.updateDestination(valeur,function(err, rows) {
                        if(err != null){
                             res.send({success: false,error:err});
                        } else {
                            res.send({success: true});
                        }
                    });
                }
            });
        }
    });
});

router.post('/supprimerDestination', function(req, res, next) {
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            res.send({ success: false, error:erreur});
        } else {
            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    var valeur = req.body;
                    valeur.idCompte = rows.idCompte;
                    bdd.supprimerDestination(valeur,function(err, rows) {
                        if(err != null){
                             res.send({success: false,error:err});
                        } else {
                            res.send({success: true});
                        }
                    });
                }
            });
        }
    });
});


// déposer l'image dans un répertoire du serveur (/public/img/imageDeProfil)

var storage = multer.diskStorage
({
    destination: function(req, file, callback)
    {
        if (req.params.path === 'imageDeProfil') {
            callback(null, 'public/img/imageDeProfil');
        } else if(req.params.path === 'imageAnnonce'){
            callback(null, 'public/img/imageAnnonce');
        } else {
            callback(null, 'none');
        }
    },
    filename: function(req, file, callback)
    {
        callback(null, req.params.id + '.jpg');
    }
});

var upload = multer({storage: storage}).single('file');

router.post('/upload/:id/:path', function(req, res, next){
    upload(req, res, function(err)
    {
        if(err) {
            res.send({error_code: 1, err_desc: err});
            return;
        }
        res.send({error_code: 0, err_desc: null});
    });
});


var transporter = nodemailer.createTransport
({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true,
    secureConnection: true,
    auth: 
    {
        user: 'inventorno1@gmail.com', 
        pass: 'vatlikithuat'
    },
    tls: {
        rejectUnauthorized: false
    }
});


router.post('/email/:param/', function(req, res)
{
    var mail;
    var key = '';
    var caractere;

    if (typeof req.body.email != "undefined") {
        bdd.getIdCompte(req.body,function(err, rows) {
            if(err != null || typeof rows == 'undefined'){
                console.log("ERROR= " +err);
            } else {
                var valeur = req.body;
                req.body.idCompte = rows.idCompte;
                for(var i = 0; i < 20; i++)
                {
                    caractere = Math.round(Math.random() * 9);
                    key += caractere;
                }

                bdd.changeKey({id: req.body, key: key}, function(err)
                {
                    if(err != null)
                        console.log(err);
                });

                if(req.params.param == 'success'){
                    mail = "Bonjour, Vous avez changé votre mot de passe sur votre compte 1Rempla. Cet email est généré automatiquement, nous vous remercions de ne pas y répondre.";
                } else if(req.params.param == 'password'){
                    mail = ' Bonjour,   Pour changer votre mot de passe sur 1Rempla.com . Veuillez cliquer sur localhost:3000/modificationMdp?idCompte=' + req.body.idCompte + '&key=' + key +' . Cet email est généré automatiquement, nous vous remercions de ne pas y répondre.';
                };

                var mailOptions = 
                {
                    from: 'inventorno1@gmail.com',
                    to: req.body.email,
                    subject: 'Changement MDP - No reply - 1Rempla',
                    text: mail
                };

                transporter.sendMail(mailOptions, function(err, info)
                {
                    if(err)
                       console.log(err);
                });
                res.end();
            }
        });
    } else {
        res.end();
    }
});

router.post('/modificationMdp', function(req, res)
{
    bdd.getKeyCompte(req.body, function(err, data)
    {
        if(err == null)
        {
            if(req.body.key == data.cleModificationMdp)
            {
                bdd.changePassword({body: req.body}, function(err)
                {
                    if(err != null)
                        console.log(err);
                });
            }
        }
    });
    res.end();
});

router.post('/modificationEmail', function(req, res, next)
{
    jwtauth(req, null, function(err, result)
    {
        if(err != null)
            res.send({success: false, error: err});
        else
        {
            bdd.updateEmailCompte({body: req.body, email: result.email}, function(error)
            {
                if(error == null)
                    res.send({success: true, error: error});
                else
                    res.send({success: false, error: null});
            });
        }
    });
});

router.post('/suppressionCompte', function(req, res, next)
{
    jwtauth(req, null, function(err, result)
    {
        if(err != null)
            res.send({success: false, error: err});
        else
        {
            bdd.getIdCompte(result, function(err, data)
            {
                fs.unlink('/img/imageDeProfil/' + data.idCompte + '.jpg', function(err) {});

                bdd.supprimerCompte(data.idCompte, function(error){
                    if(error != null)
                        res.send({success: false, error: error});
                    else
                        res.send({success: true, error: null});
                });
            });
            
        }
    });
});


router.post('/suppressionAdmin', function(req, res, next){
    var result = req.body.idCompte;
    fs.unlink('/img/imageDeProfil/' + result + '.jpg', function(err) {});
    bdd.supprimerCompte(result, function(error){
        if(error != null){
            res.send({success: false, error: error});
        }else{
            res.send({success: true, error: null});
        };
    });
});

router.post('/suppressionImageAnnonce', function(req, res, next) {
    if (typeof req.body.imageSupprimer != "undefined") {
        jwtauth(req, null, function(err, result)
        {
            if(err != null)
                res.send({success: false, error: err});
            else {
                bdd.getIdCompte(result, function(err, data) {
                    if (typeof data.idCompte != "undefined") {
                        fs.unlink('public/img/imageAnnonce/' + data.idCompte + "_" + req.body.imageSupprimer + '.jpg', function(err) {});
                        res.send({success: true, error: null});
                    };
                });
            }
        });
    } else {
        res.send({success: false, error: "Parametres invalides"});
    }
});


router.post('/addAvis',function(req,res,next){
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            console.log("ERR = "+err);
            res.send({ success: false, error:erreur});
        } else {

            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    valeur = {
                        idCompte1: rows.idCompte,       //emeteur
                        idCompte2: req.body.idCompte2,  //recepteur
                        note : req.body.note,
                        message : req.body.message
                    };
                    bdd.addAvis(valeur,function(err, row){
                        if(err != null){
                            res.send(false);
                        } else {
                            res.send(true);
                        }     
                    });
                }
            });
        }
    });
});

router.post('/getAvis',function(req,res,next){
    valeur = {
        idCompte2:req.body.idCompte2
    };
    bdd.getAvis(valeur,function(err,row){
        if(err != null){
            console.log("ERROR= " +err);
        } else {
            res.send({ success: true, res:row});
        }
    });
});

router.post('/addReport',function(req,res,next){
    jwtauth(req,null,function(erreur, resultat){
        if(erreur != null){
            console.log("ERR = "+err);
            res.send({ success: false, error:erreur});
        } else {

            bdd.getIdCompte(resultat,function(err, rows) {
                if(err != null || typeof rows == 'undefined'){
                    console.log("ERROR= " +err);
                } else {
                    valeur = {
                        idCompte1: rows.idCompte,       //emeteur
                        idCompte2: req.body.idCompte2,  //recepteur
                        message : req.body.message
                    }; 
                    bdd.addReport(valeur,function(err, row){
                        if(err != null){
                            res.send(false);
                        } else {
                            res.send(true);
                        }     
                    });
                }
            });
        }
    });
});

router.post('/getReport',function(req,res,next){
    bdd.getReport(function(err,row){
        if(err != null){
            console.log("ERROR= " +err);
        } else {
            res.send({ success: true, res:row});
        };
    });
});


module.exports = router;