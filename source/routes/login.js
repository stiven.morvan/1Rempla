var express = require('express');
var jwt = require('jwt-simple');
var bdd = require('../models/bdd/action_bdd');
var router = express.Router();
var app = require('../app');
var jwtauth = require('./jwtauth.js');

router.post('/', function(req, res, next){
  	bdd.checkMdp(req.body,function(err,rows){
  		if(err != null){
        res.send({sucess: false, erreur: err});
      } else{
        if(rows.mdpBon == true){
          
        	var token = jwt.encode({
            email: req.body.email,
    		    mdp: rows.mdp[0].mdp
    			}, app.get('jwtTokenSecret'));
    			
    			res.json({
            sucess: true,
            erreur: null,
    				token : token
    			});

        } else if(rows.emailInexistant == true){
          res.json({
            sucess: false,
            erreur: {mdpBon:false,emailInexistant:true},
            token : null
          });
        } else{
          res.json({
            sucess: false,
            erreur: {mdpBon:false,emailInexistant:false},
            token : null
          });
        }
      }
  	});
});

module.exports = router;