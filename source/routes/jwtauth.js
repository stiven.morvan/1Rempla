//var UserModel = require('../models/user');
var express = require('express');
var jwt = require('jwt-simple');
var bdd = require('../models/bdd/action_bdd');
var router = express.Router();
var app = require('../app');
//var bodyParser = require('body-parser');

module.exports = function(req, res, callback) {

   var token = req.headers['x-access-token'];

   if (token) {
    try {

      var decoded = jwt.decode(JSON.parse(token).token, app.get('jwtTokenSecret'));

      // On verifie le compte
      bdd.checkMdpAlreadyHashed(decoded,function(err,rows){
        if(err != null){
          callback(err);
        } else{

          if(rows.mdpBon == true){

            callback(null,{email: decoded.email});

          } else{
            
            callback({"err":"Les identifiant ne sont pas conforme"});

          }
        }
      });

    } catch (err) {
      
      callback(err);

    }
  } else {
    callback(true);
  }
};