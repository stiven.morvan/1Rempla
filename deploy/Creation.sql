## autor Sevin Evan
#############################
#### Creation de table ######
#############################

### Compte ###
CREATE TABLE Compte (
  idCompte        int(10) NOT NULL AUTO_INCREMENT, 
  nom             varchar(30) NOT NULL,
  prenom          varchar(30) NOT NULL, 
  description     varchar(3000), 
  dateDeNaissance date NOT NULL CHECK(dateDeNaissance  <= DATE_SUB(CURDATE(),INTERVAL 18 YEAR)), 
  idProfessionnel bigint NOT NULL, 
  numTel          varchar(10), 
  email           varchar(100) NOT NULL UNIQUE, 
  mdp             varchar(64) NOT NULL, 
  actif           tinyint NOT NULL CHECK(actif = 0 OR actif = 1), 
  specialite      varchar(100) NOT NULL,
  cleModificationMdp varchar(20),
  PRIMARY KEY (idCompte));

### Avis ###
CREATE TABLE Avis (
  idCompte1 int(10) NOT NULL, 
  idCompte2 int(10) NOT NULL, 
  message   varchar(3000), 
  note      int(1) NOT NULL CHECK(note <= 5 AND note>=0), 
  PRIMARY KEY (idCompte1, 
  idCompte2));

### Signalement ###
CREATE TABLE Signalement (
  idCompte1 int(10) NOT NULL, 
  idCompte2 int(10) NOT NULL, 
  message   varchar(3000), 
  PRIMARY KEY (idCompte1, 
  idCompte2));

### TypeStructure ###
CREATE TABLE TypeStructure (
  typeStructure varchar(100) NOT NULL, 
  PRIMARY KEY (typeStructure));

### LieuDeTravail ###
CREATE TABLE LieuDeTravail (
  idLieu        int(10) NOT NULL AUTO_INCREMENT, 
  titre         varchar(100) NOT NULL, 
  description   varchar(3000), 
  nbImage       int(2) NOT NULL CHECK(nbImage>0 AND nbImage<10), 
  informatise   tinyint NOT NULL CHECK(informatise=0 OR informatise=1), 
  secretariat   tinyint NOT NULL CHECK(secretariat = 0 OR secretariat = 1), 
  logement      tinyint NOT NULL CHECK(logement = 0 OR logement =1), 
  ville         varchar(100) NOT NULL, 
  adresse       varchar(300) NOT NULL, 
  lat           double NOT NULL  CHECK(lat >= -90.0 AND lat <= 90.0), 
  lon           double NOT NULL  CHECK(lon >= -180.0 AND lon <= 180.0), 
  codePostal    int(5) NOT NULL, 
  typeStructure varchar(100), 
  CompteLier   int(10) NOT NULL, 
  PRIMARY KEY (idLieu));

### Remplace ###
CREATE TABLE Remplace (
  idCompte int(10) NOT NULL, 
  PRIMARY KEY (idCompte));

### Remplacant ###
CREATE TABLE Remplacant (
  idCompte      int(10) NOT NULL, 
  descriptionCV varchar(3000), 
  ville         varchar(100), 
  adresse       varchar(300), 
  PRIMARY KEY (idCompte));

### DemandeDeRemplacement ###
CREATE TABLE DemandeDeRemplacement (
  idCompteRemplacant int(10) NOT NULL, 
  idAnnonce          int(10) NOT NULL, 
  dateA              datetime NOT NULL DEFAULT NOW(), 
  PRIMARY KEY (idCompteRemplacant,idAnnonce));

### Destination ###
CREATE TABLE Destination (
  idDestination int(10) NOT NULL AUTO_INCREMENT,
  lat double NOT NULL  CHECK(lat >= -90.0 AND lat <= 90.0), 
  lng double NOT NULL  CHECK(lon >= -180.0 AND lon <= 180.0), 
  idCompte int(10) NOT NULL, 
  rayon    int(10) DEFAULT 1000, 
  PRIMARY KEY (idDestination));

### Annonce ###
CREATE TABLE Annonce (
  idAnnonce          int(10) NOT NULL AUTO_INCREMENT, 
  debut              date NOT NULL, 
  fin                date NOT NULL, 
  idCompteRemplacant int(10), 
  idCompteRemplace   int(10) NOT NULL, 
  PRIMARY KEY (idAnnonce));

### Specialisation ###
CREATE TABLE Specialite (
  specialite varchar(100) NOT NULL, 
  PRIMARY KEY (specialite));

### Moderateur ###
CREATE TABLE Moderateur (
  idCompte       int(10) NOT NULL, 
  administrateur int(1) NOT NULL CHECK(administrateur = 0 OR administrateur = 1), 
  PRIMARY KEY (idCompte));

### Equipement ###
CREATE TABLE Equipement (
  equipement varchar(100) NOT NULL, 
  PRIMARY KEY (equipement));

### ACOMME (table assotiation) ###
CREATE TABLE AComme (
  equipement      varchar(100) NOT NULL, 
  idLieuDeTravail int(10) NOT NULL, 
  PRIMARY KEY (equipement, 
  idLieuDeTravail));

###################################
### Contrainte de clé etrangère ###
###################################

### Entre LieuDeTravail et TypeStructure ### Nom de la contrainte = (FK_LieuDeTravail_typeStructure) ###
ALTER TABLE LieuDeTravail ADD INDEX FK_LieuDeTravail_typeStructure (typeStructure), ADD CONSTRAINT FK_LieuDeTravail_typeStructure FOREIGN KEY (typeStructure) REFERENCES TypeStructure (typeStructure);

### Entre Remplacé et Compte ### Nom de la contrainte = (FK_Remplace_idCompte) ###
ALTER TABLE Remplace ADD INDEX FK_Remplace_idCompte (idCompte), ADD CONSTRAINT FKRemplace_idCompte FOREIGN KEY (idCompte) REFERENCES Compte (idCompte);

### Entre Remplacant et Compte ### Nom de la contrainte = (FK_Remplacant_idCompte) ###
ALTER TABLE Remplacant ADD INDEX FK_Remplacant_idCompte (idCompte), ADD CONSTRAINT FK_Remplacant_idCompte FOREIGN KEY (idCompte) REFERENCES Compte (idCompte);

### Entre DemandeDeRemplacement et Ramplacant ### Nom de la contrainte = (FK_DemandeDeRemplacement_idCompteRemplacant) ###
ALTER TABLE DemandeDeRemplacement ADD INDEX FK_DemandeDeRemplacement_idCompteRemplacant (idCompteRemplacant), ADD CONSTRAINT FK_DemandeDeRemplacement_idCompteRemplacant FOREIGN KEY (idCompteRemplacant) REFERENCES Remplacant (idCompte);

### Entre DemandeDeRemplacememnt et Remplacé ### Nom de la contrainte = (FK_DemandeDeRemplacement_idCompteRemplace) ###
ALTER TABLE DemandeDeRemplacement ADD INDEX FK_DemandeDeRemplacement_idCompteRemplace (idAnnonce), ADD CONSTRAINT FK_DemandeDeRemplacement_idCompteRemplace FOREIGN KEY (idAnnonce) REFERENCES Annonce (idAnnonce);

### Entre Destination et Compte ### Nom de la contrainte = (FK_Destination_idCompte) ###
ALTER TABLE Destination ADD INDEX FK_Destination_idCompte (idCompte), ADD CONSTRAINT FK_Destination_idCompte FOREIGN KEY (idCompte) REFERENCES Remplacant (idCompte);

### Entre Compte et Specialite ### Nom de la contrainte = (FK_Compte_specialite) ###
ALTER TABLE Compte ADD INDEX FK_Compte_specialite (specialite), ADD CONSTRAINT FK_Compte_specialite FOREIGN KEY (specialite) REFERENCES Specialite (specialite);

### Entre Signalement et Compte(emetteur) ### Nom de la contrainte =  ###
ALTER TABLE Signalement ADD INDEX FK_Signalement_idCompte1 (idCompte1), ADD CONSTRAINT FK_Signalement_idCompte1 FOREIGN KEY (idCompte1) REFERENCES Compte (idCompte);

### Entre Signalement et Compte(receveur) ### Nom de la contrainte = (FK_Signalement_idCompte1) ###
ALTER TABLE Signalement ADD INDEX FK_Signalement_idCompte2 (idCompte2), ADD CONSTRAINT FK_Signalement_idCompte2 FOREIGN KEY (idCompte2) REFERENCES Compte (idCompte);

### Entre Avis et Compte(emetteur) ### Nom de la contrainte = (FK_Avis_idCompte1) ###
ALTER TABLE Avis ADD INDEX FK_Avis_idCompte1 (idCompte1), ADD CONSTRAINT FK_Avis_idCompte1 FOREIGN KEY (idCompte1) REFERENCES Compte (idCompte);

### Entre Avis et Compte(receveur) ### Nom de la contrainte = (FK_Avis_idCompte2) ###
ALTER TABLE Avis ADD INDEX FK_Avis_idCompte2 (idCompte2), ADD CONSTRAINT FK_Avis_idCompte2 FOREIGN KEY (idCompte2) REFERENCES Compte (idCompte);

### Entre Annonce et Remplacant ### Nom de la contrainte = (FK_Annonce_idCompteRemplacant) ###
ALTER TABLE Annonce ADD INDEX FK_Annonce_idCompteRemplacant (idCompteRemplacant), ADD CONSTRAINT FK_Annonce_idCompteRemplacant FOREIGN KEY (idCompteRemplacant) REFERENCES Remplacant (idCompte);

### Entre Moderateur et Compte ### Nom de la contrainte = (FK_Moderateur_idCompte) ###
ALTER TABLE Moderateur ADD INDEX FK_Moderateur_idCompte (idCompte), ADD CONSTRAINT FK_Moderateur_idCompte FOREIGN KEY (idCompte) REFERENCES Compte (idCompte);

### Entre Annonce et Remplacé ### Nom de la contrainte = (FK_Annonce_idCompteRemplace) ###
ALTER TABLE Annonce ADD INDEX FK_Annonce_idCompteRemplace (idCompteRemplace), ADD CONSTRAINT FK_Annonce_idCompteRemplace FOREIGN KEY (idCompteRemplace) REFERENCES Remplace (idCompte);

### Entre LieuDeTravail et Remplace ### Nom de la contrainte = (FK_LieuDeTravail_idCompteRemplace) ###
ALTER TABLE LieuDeTravail ADD INDEX FK_LieuDeTravail_idCompteRemplace (CompteLier), ADD CONSTRAINT FK_LieuDeTravail_idCompteRemplace FOREIGN KEY (CompteLier) REFERENCES Remplace (idCompte);

### Entre Acomme et Equipement ### Nom de la contrainte = (FK_AComme_equipement) ###
ALTER TABLE AComme ADD INDEX FK_AComme_equipement (equipement), ADD CONSTRAINT FK_AComme_equipement FOREIGN KEY (equipement) REFERENCES Equipement (equipement);

### Entre AComme et LieuDeTravail ### Nom de la contrainte = (FK_AComme_LieuDeTravail) ###
ALTER TABLE AComme ADD INDEX FK_AComme_LieuDeTravail (idLieuDeTravail), ADD CONSTRAINT FK_AComme_LieuDeTravail FOREIGN KEY (idLieuDeTravail) REFERENCES LieuDeTravail (idLieu);
