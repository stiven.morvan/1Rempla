############################################
#### Installation de la base de donn�es ####
############################################
Voici un exemple d'installation pour la base de donn�es sur une machine.

-> Installez un serveur mysql sur votre machine.


-> Une fois installer, modifier la variable PATH de votre machine pour inclure le r�pertoire bin du repertoire du serveur mysql. Exemple pour Windows =
set PATH=%PATH%;C:\"Program Files"\MySQL\"MySQL Server 5.7"\bin


-> Connectez vous au serveur :
mysql -h localhost --user=root -p


-> Puis dans l'invite de commande mysql :

CREATE DATABASE mediweb CHARACTER SET 'utf8';
use mediweb;


-> Enfin, executez les diff�rents scripts :

source monCheminVers/Creation.sql;
source monCheminVers/Trigger.sql;

Pour ins�rer les tuples de test faites un copier coller du contenu du fichier InsertionPourLeDevellopement.sql

-> Pour verifier le bon fonctionnement vous pouvez utiliser ces commandes :

SHOW TABLES;      -- liste les tables de la base de donn�es
DESCRIBE NomDuneTable;  -- liste les colonnes de la table avec leurs caract�ristiques