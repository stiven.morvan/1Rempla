## Auteur: Pham Hai Trung
## Chargé de test
## Projet: Médiweb
## Chef de projet: Stiven Morvan
## Client: Dr. Nicolas Thoual

########################################################
##- Initialisation ####################################-
########################################################

## Insération des tuples dans la table 'Specialite'

INSERT INTO Specialite
VALUES ('Assistant de service social');
INSERT INTO Specialite
VALUES ('Audio-prothésiste');
INSERT INTO Specialite
VALUES ('Diététicien');
INSERT INTO Specialite
VALUES ('Epithésiste');
INSERT INTO Specialite
VALUES ('Ergothérapeute');
INSERT INTO Specialite
VALUES ('Infirmier');
INSERT INTO Specialite
VALUES ('Manipulateur en radiologie');
INSERT INTO Specialite
VALUES ('Masseur-kinésithérapeute');
INSERT INTO Specialite
VALUES ('Oculariste');
INSERT INTO Specialite
VALUES ('Ostéopathes');
INSERT INTO Specialite
VALUES ('Opticien-lunetier');
INSERT INTO Specialite
VALUES ('Orthopédiste-orthésiste');
INSERT INTO Specialite
VALUES ('Orthophoniste');
INSERT INTO Specialite
VALUES ('Orthoprothésiste');
INSERT INTO Specialite
VALUES ('Orthoptiste');
INSERT INTO Specialite
VALUES ('Pédicure-podologue');
INSERT INTO Specialite
VALUES ('Podo-orthésiste');
INSERT INTO Specialite
VALUES ('Psychomotricien');
INSERT INTO Specialite
VALUES ('Psychologue');
INSERT INTO Specialite
VALUES ('Médecin');
INSERT INTO Specialite
VALUES ('Chirurgien-dentiste');
INSERT INTO Specialite
VALUES ('Pharmacien');
INSERT INTO Specialite
VALUES ('Psychothérapeute');
INSERT INTO Specialite
VALUES ('Dentiste');

## Insération des tuples dans la table 'TypeStructure'

INSERT INTO TypeStructure
VALUES ('Cabinet seul');
INSERT INTO TypeStructure
VALUES ('Clinic');
INSERT INTO TypeStructure
VALUES ('Hopital');
INSERT INTO TypeStructure
VALUES ('MSP-PSP');
INSERT INTO TypeStructure
VALUES ('Autre');
INSERT INTO TypeStructure
VALUES ('typeStructure');