### crée la table d'erreur ###
CREATE TABLE Error (
error varchar(1000) PRIMARY KEY
);

### insertion tuple d'erreur ###
INSERT INTO Error (error) VALUES ("Un remplacant ne peut pas etre ramplace");
INSERT INTO Error (error) VALUES ("Un remplace ne peux pas etre remplacant");
INSERT INTO Error (error) VALUES ("la date de fin doit etre superieur à la date de debut");
INSERT INTO Error (error) VALUES ("un compte ne peu pas se signaler");
INSERT INTO Error (error) VALUES ("un compte ne peu pas se donner d'avis");


DELIMITER |
CREATE TRIGGER INS_RemplacantORRemplace 
BEFORE INSERT
ON Remplacant FOR EACH ROW
	
		BEGIN
		DECLARE var int;
		SELECT Remplace.idCompte into var
		FROM Remplace
		WHERE NEW.idCompte = Remplace.idCompte;
		
		IF (var IS NOT NULL) THEN
			INSERT INTO Error (error) VALUES ("Un remplacant ne peut pas etre ramplace");
		END IF;
	END;

CREATE TRIGGER UPD_RemplacantORRemplace 
BEFORE UPDATE
ON Remplacant FOR EACH ROW
	
		BEGIN
		DECLARE var int;
		SELECT Remplace.idCompte into var
		FROM Remplace
		WHERE NEW.idCompte = Remplace.idCompte;
		
		IF (var IS NOT NULL) THEN
			INSERT INTO Error (error) VALUES ("Un remplacant ne peut pas etre ramplace");
		END IF;
	END;
|
DELIMITER ;



DELIMITER |
CREATE TRIGGER INS_RemplaceORRemplacant 
BEFORE INSERT
ON Remplace FOR EACH ROW
		BEGIN
		DECLARE var int;
		SELECT Remplacant.idCompte into var
		FROM Remplacant
		WHERE NEW.idCompte = Remplacant.idCompte;
		
		IF (var IS NOT NULL) THEN
			INSERT INTO Error (error) VALUES ("Un remplace ne peux pas etre remplacant");
		END IF;
	END;
|
CREATE TRIGGER UPD_RemplaceORRemplacant 
BEFORE UPDATE
ON Remplace FOR EACH ROW
		BEGIN
		DECLARE var int;
		SELECT Remplacant.idCompte into var
		FROM Remplacant
		WHERE NEW.idCompte = Remplacant.idCompte;
		
		IF (var IS NOT NULL) THEN
			INSERT INTO Error (error) VALUES ("Un remplace ne peux pas etre remplacant");
		END IF;
	END;
|
DELIMITER ;



DELIMITER |
CREATE TRIGGER INS_Date_debut_fin 
BEFORE INSERT
ON Annonce FOR EACH ROW
	BEGIN

		IF(NEW.debut>NEW.fin) THEN
			INSERT INTO Error (error) VALUES ("la date de fin doit etre superieur à la date de debut");
		END IF;	

	END;
|
CREATE TRIGGER UPD_Date_debut_fin 
BEFORE INSERT
ON Annonce FOR EACH ROW
	BEGIN

		IF(NEW.debut>NEW.fin) THEN
			INSERT INTO Error (error) VALUES ("la date de fin doit etre superieur à la date de debut");
		END IF;	

	END;
|
DELIMITER ;



DELIMITER |
CREATE TRIGGER INS_autoAvis
BEFORE INSERT
ON Avis FOR EACH ROW
	
	BEGIN
		IF(NEW.idCompte1 = NEW.idCompte2) THEN
			INSERT INTO Error (error) VALUES ("un compte ne peu pas se donner d'avis");
		END IF;	

	END;
|

CREATE TRIGGER UPD_autoAvis
BEFORE UPDATE
ON Avis FOR EACH ROW
	
	BEGIN
		IF(NEW.idCompte1 = NEW.idCompte2) THEN
			INSERT INTO Error (error) VALUES ("un compte ne peu pas se donner d'avis");
		END IF;	

	END;
|
DELIMITER ;





DELIMITER |
CREATE TRIGGER INS_autoSignalement
BEFORE INSERT
ON Signalement FOR EACH ROW
	BEGIN
		IF(NEW.idCompte1 = NEW.idCompte2) THEN
			INSERT INTO Error (error) VALUES ("un compte ne peu pas se signaler");
		END IF;	

	END;
|
CREATE TRIGGER UPD_autoSignalement
BEFORE UPDATE
ON Signalement FOR EACH ROW
	BEGIN
		IF(NEW.idCompte1 = NEW.idCompte2) THEN
			INSERT INTO Error (error) VALUES ("un compte ne peu pas se signaler");
		END IF;	

	END;
|
DELIMITER ;
